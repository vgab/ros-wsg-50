# Software contribution guidelines
## Workflow

- **clone the repository**: If you haven't done it.
- **create a gitlab issue**: create an issue that describes the work you are going to do. It should contain a description of what the task is about (at least a brief one). Some discussion can take place here if other partners have something to say about the task.
- **create a branch to work on**: create a branch form the latest version of the main (master) branch. You will work on this new branch. e.g. if you are going to work on `issue #5` and it is a feature issue then you would do:

```
git checkout -b F#5_some_descriptive_name origin/master
```

- **commit your changes**: to your newly created branch

```
git add a_new_file_you_are_addding
git commit -am "Explanation of the changes made. It should be descriptive enough"
```

- **merge the main branch into yours**: While you were working on your branch, other people may have merged other changes into the main branch (through PR, of course ;). To make sure that your changes are compatible with those (and to solve any merging problems that could derive from that), you have to merge the main branch into your branch:

```
git merge origin/master
```

If there are merge conflicts use `git status` and follow the instructions it shows. Generally you will need to open and manually fix the files where the conflicts happened, then add those files and commit.

```
git merge origin/master
#some conflicts!!
git status
#edit file_1 to fix the conflict
git add file_1
git commit
```

- **make sure that the workspace still compiles**: It seems like an obvious requirement but please do it. Go to the root of your ROS workspace and do catkin_make:


Of course if there are compilation issues you should fix them before doing a PR.

- **push your branch to remote (origin)** the name is the name of your branch

```
git push origin F#5_some_descriptive_name
```

- **do a merge request**

- **add reference to issue**: If your MR is the solution of an issue (e.g. issue #5), you should include in your PR comment text `closes #5`. This will automatically close that issue when the MR is accepted and merged into the main branch.

- **fixing merge request issues**: During merge request review people might ask you to fix issues which they have spotted in your code or might have some other remarks.
After addressing all those issues and remarks please create comment in your merge request which is saying that it is ready for one more review.
In this case reviewers will know that they can check your merge request again.



## Coding guidelines
- **do NOT push your code to the main branch**: no code that has not been reviewed by someone else can be pushed to the main branch. When you start a feature, you create a new branch (see in the *Conventions* for a good name for that branch) and you keep working in that branch until you're satisfied the feature is ready, documented, etc... then you send a merge request and wait for someone else to review the code and merge it.
- **keep the merge requests as focused as possible**: merging long merge request is very painful. Keep them as short as possible. It's much better to have multiple smaller merge requests to merge than a big one.
- **cosmetics merge requests should be separate**: if for some reason you need to do some cosmetic change (whitespace, indentation, whatever), please do so in a separate code request. If you inadvertently introduced some noise there are some tricks (see the *Useful tricks* section) to remove that from your commit.
- **accidentally merged merge request**: in this case please create another merge request which reverts your merge.

## Conventions

### Branch naming

The recommended convention is:
- When solving a bug `B#12_descriptive_name`
- When adding a feature `F#13_descriptive name`

Where #number is a reference to the github issue that this branch is going to address.


### Code formatting
We tend to stick to the ROS_format convention used in Eclipse. Basically make sure you:
 - don't leave trailing whitespaces at the end of your lines
 - go to the next line before a bracket (c++ obviously)
 - use CamelCase for Classes and underscored_names for functions / variables.
