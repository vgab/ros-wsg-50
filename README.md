# ROS package for Schunk WSG-50 Gripper

Forked from: [https://code.google.com/p/wsg50-ros-pkg](https://code.google.com/p/wsg50-ros-pkg)

Driver support is currently mainly focussing on ```script``` communication via Ethernet and TCP. 
CAN support is more or less dropped, but previous file remain to be found in ```wsg_50_hw``` package


# Overview

- [wsg_50_description](./wsg_50_description) contains URDF-models and CAD-meshes
- [wsg_50_gazebo](./wsg_50_gazebo) contains a simulated version of the gripper. **work in progress / use with care**
- [wsg_50_msgs](./wsg_50_msgs) contains custom messages and services in use
- [wsg_50_hw](./wsg_50_hw) contains the actual ros-driver that wraps both the hardware into a ros-conform API, while also allowing to spawn the gripper as a standalone ros-node.
