#include <signal.h>
#include <thread>

#include <wsg_50_hw/wsg_50_api.h>
#include <wsg_50_hw/wsg_50_ros.h>

using namespace wsg_50;


Wsg50Ros ros_if{};
Wsg50RosSrvs ros_srvs{};

void sigint_handler(int sig) {
    ROS_INFO("Exiting...");

    ros::shutdown();
}

/**
 * The main function
 */


struct WsgNodeParams{
    std::string joint_state_topic{"joint_states"};
    std::vector<std::string> joints{"wsg_50_gripper_base_joint_gripper_left",
                                    "wsg_50_gripper_base_joint_gripper_right"};
    std::string frame_id{"wsg_50_gripper_base_link"};
    double loop_hz{5.0};

    WsgNodeParams(const std::string p_n=""){
        ros::NodeHandle nh{"~"};
        std::string base_param{p_n};
        nh.param(base_param + "joint_names", joints, joints);
        nh.param<std::string>(base_param + "frame_id", frame_id, frame_id);
        nh.param<std::string>(base_param + "joint_state_topic", joint_state_topic, joint_state_topic);
        nh.param<double>(base_param + "loop_hz", loop_hz, loop_hz);
    }

    inline std::string info() const{
        std::stringstream tmp;
        if (joints.size() >1){
            tmp << "\n\tleft joint:                   \t" << joints[0]
                << "\n\tright joint:                  \t" << joints[1];
        }
        tmp << "\n\tframe_id:                     \t" << frame_id
            << "\n\tloop_hz:      [1/s]           \t" << loop_hz
            << "\n\tjoint state topic:            \t" << joint_state_topic;
        return tmp.str();
    }
};



int main(int argc, char **argv) {
    ros::init(argc, argv, "wsg_50_hw");
    ros::AsyncSpinner spinner(4);
    spinner.start();
    ros::NodeHandle nh("~");
    signal(SIGINT, sigint_handler);
    std::shared_ptr<Wsg50API> gripper_if = std::make_shared<Wsg50API>();
    WsgNodeParams params{};

    if (!gripper_if->connect(nh)){
        ROS_ERROR("[%s] Failed to connect to gripper. Exiting", ros::this_node::getName().c_str());
        exit(1);
    }
    if (!ros_srvs.init(gripper_if, nh) || !ros_if.init(gripper_if, nh)){
        ROS_ERROR("[%s] Failed to set up ros interfaces. Exiting", ros::this_node::getName().c_str());
        exit(1);
    }

    ros::Rate rate(params.loop_hz);
    sensor_msgs::JointState joint_states;

    std::shared_ptr<ros::Publisher> pub_status = std::make_shared<ros::Publisher>(nh.advertise<wsg_50_msgs::Status>("status", 1000));
    std::shared_ptr<ros::Publisher> pub_moving = std::make_shared<ros::Publisher>(nh.advertise<std_msgs::Bool>("moving", 10));
    std::shared_ptr<ros::Publisher> pub_joints = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::JointState>(params.joint_state_topic, 10));
    joint_states.header.frame_id = params.frame_id;
    assert(params.joints.size() > 1);
    joint_states.name.assign(params.joints.begin(), params.joints.begin() + 2);
    joint_states.position.resize(2);
    joint_states.velocity.resize(2);
    joint_states.effort.resize(2);

    ROS_DEBUG("[WSG 50] Started gripper node: %s", params.info().c_str());
    auto prev = ros::Time::now();
    std_msgs::Bool mmsg;
    while(ros::ok()){
        auto now = ros::Time::now();
        auto dt = now - prev;
        gripper_if->update();
        joint_states.header.stamp = ros::Time::now();
        std::tie(joint_states.position[0],
                    joint_states.position[1]) = gripper_if->getJointPosition();
        double s = (gripper_if->getCurrentSpeed());
        double f = (gripper_if->getMotorForce());
        joint_states.velocity[0] = s;
        joint_states.velocity[1] = s;
        joint_states.effort[0] = f;
        joint_states.effort[1] = f;
        pub_joints->publish(joint_states);
        pub_status->publish(getStatusMsg(gripper_if));
        mmsg.data = gripper_if->isMoving();
        pub_moving->publish(mmsg);
        rate.sleep();
        prev = now;
    }
}
