//
// Created by gabler on 27.01.21.
//
#include <chrono>
#include <wsg_50_hw/wsg_50_ros.h>


namespace wsg_50 {
    // -- HELPER FUNCTIONS
    bool assert_hw(const std::shared_ptr<wsg_50::Wsg50API> &wsg_api, ros::NodeHandle &nh) {
        if (wsg_api == nullptr) {
            ROS_ERROR("[WSG 50] gripper not initialized!");
            return false;
        }
        if (!wsg_api->isConnected()){
            if (wsg_api->connect(nh))
                return true;
            ROS_ERROR("[WSG50] failed connection to gripper");
            return false;
        }
        return true;
    };

    bool preProcessSrv(const std::shared_ptr<wsg_50::Wsg50API> &wsg_api){
        if (wsg_api){
            if (!wsg_api->isConnected()){
                ROS_ERROR("[WSG50] no connection to gripper");
                return false;
            }
            return true;
        }
        ROS_ERROR("[WSG50] interface not initialized");
        return false;

    }

    bool checkWidth(const float &w) {
        return (GRIPPER_MIN_OPEN <= w) && (w <= GRIPPER_MAX_OPEN);
    }

    bool checkSpeed(const float &s) {
        return (GRIPPER_MIN_SPEED <= s) && (s <= GRIPPER_MAX_SPEED);
    }

    float clampSpeed(const float &s) {
        if (!checkSpeed(s)) {
            ROS_WARN("[WSG 50]  Speed values are outside the gripper's physical limits ([0.1 - 420.0])  Using clamped values.");
            return std::max(GRIPPER_MIN_SPEED, std::min(s, GRIPPER_MAX_SPEED));
        }
        return s;
    }

    std::tuple<bool, float, float> adjustGripperValues(const float &w, float &s) {
        if (checkWidth(w)) {
            s = clampSpeed(s);
            return std::make_tuple(true, w, s);
        }
        ROS_WARN("[WSG 50] Width exceeds legal limits. Aborting.");
        return std::make_tuple(false, w, s);
    }

    // public functions
    wsg_50_msgs::Status getStatusMsg(const std::shared_ptr<wsg_50::Wsg50API> &hw_api) {
        wsg_50_msgs::Status status_msg;
        status_msg.status = hw_api->getStatus();
        status_msg.width = hw_api->getOpenWidth();
        status_msg.speed = hw_api->getCurrentSpeed();
        status_msg.acc = hw_api->getAcceleration();
        status_msg.force = hw_api->getMotorForce();
        std::tie(status_msg.force_finger0, status_msg.force_finger1) = hw_api->getFingerForce();
        return status_msg;
    }

    wsg_50_msgs::StatusFull getStatusFullMsg(const std::shared_ptr<wsg_50::Wsg50API> &hw_api) {
        wsg_50_msgs::StatusFull out;
        out.status = getStatusMsg(hw_api);
        auto dsa_fingers = hw_api->getDsaDataPtr();
        out.dsa_finger_0 = asMsg(dsa_fingers.first);
        out.dsa_finger_1 = asMsg(dsa_fingers.second);
        return out;
    }

    wsg_50_msgs::DsaData asMsg(const DsaMat &dsa_data) {
        wsg_50_msgs::DsaData out{};
        out.height = DSA_ROWS;
        out.width = DSA_COLS;
        out.dsa_values.reserve(DSA_ROWS * DSA_COLS);
        for (const auto &row: dsa_data){
            out.dsa_values.insert(out.dsa_values.end(), row.begin(), row.end());
        }
        return out;
    }

    wsg_50_msgs::DsaCop asCopMsg(const std::shared_ptr<wsg_50::Wsg50API> &hw_api, const int &finger){
        wsg_50_msgs::DsaCop out{};
        std::tie(out.x, out.y) = hw_api->getDSACop(finger);
        for (auto &arr: hw_api->getDSAEdges(finger)){
            out.edge_y.push_back(arr.at(0));
            out.edge_y.push_back(arr.at(0));
            out.edge_x.push_back(arr.at(1));
            out.edge_x.push_back(arr.at(2));
        }
        out.sum = hw_api->getDSAPressureSum(finger);
        return out;
    }



    // WSG50 SERVICES
    bool Wsg50RosBaseSrvs::init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh) {
        if (!assert_hw(hw, nh)){
            return false;
        }
        m_hw_api = hw;

        // Services
        m_ack_srv = nh.advertiseService("ack", &Wsg50RosBaseSrvs::ackSrv, this);
        m_set_acc_srv = nh.advertiseService("set_acceleration", &Wsg50RosBaseSrvs::setAccSrv, this);
        m_set_force_lim_srv = nh.advertiseService("set_force_limit", &Wsg50RosBaseSrvs::setForceSrv, this);
        m_homing_srv = nh.advertiseService("homing", &Wsg50RosBaseSrvs::homingSrv, this);
        ROS_INFO("[%s] started ROS-gripper basic services", m_log_name.c_str());
        return true;
    }

    bool Wsg50RosBaseSrvs::homingSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            ROS_DEBUG("[%s]Homing...", m_log_name.c_str());
            return m_hw_api->homingCmd();
        }
        return false;
    }


    bool Wsg50RosBaseSrvs::setAccSrv(wsg_50_msgs::Conf::Request &req, wsg_50_msgs::Conf::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            try { return m_hw_api->setAcc(req.val);} catch (...) {}
        }
        return false;
    }

    bool Wsg50RosBaseSrvs::setForceSrv(wsg_50_msgs::Conf::Request &req, wsg_50_msgs::Conf::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            try {return m_hw_api->setForceLimit(req.val);} catch (...) {}
        }
        return false;
    }

    bool Wsg50RosBaseSrvs::ackSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            try {return m_hw_api->ackErr();} catch (...) {}
        }
        return false;
    }


    bool Wsg50RosSrvs::init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh) {
        if (!Wsg50RosBaseSrvs::init(hw, nh))
            return false;

        // Services
        m_move_srv = nh.advertiseService("move", &Wsg50RosSrvs::moveSrv, this);
        m_grasp_srv = nh.advertiseService("grasp", &Wsg50RosSrvs::graspSrv, this);
        m_release_srv = nh.advertiseService("release", &Wsg50RosSrvs::releaseSrv, this);        
        m_stop_srv = nh.advertiseService("stop", &Wsg50RosSrvs::stopSrv, this);
        m_move_incr_srv = nh.advertiseService("move_incrementally", &Wsg50RosSrvs::incrementSrv, this);
        ROS_INFO("[%s] started ROS-gripper services", m_log_name.c_str());
        return true;
    }



    bool Wsg50RosSrvs::moveSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            auto[legal, width, speed] = adjustGripperValues(req.width, req.speed);
            if (legal) {
                ROS_INFO("[%s] Moving to %.2f [mm] opening width at %.2f mm/s.", m_log_name.c_str(), width, speed);
                if (m_hw_api->moveCmd(width, speed, false)) {
                    ROS_DEBUG("[%s]Target position reached.", m_log_name.c_str());
                    return true;
                }
            } else {
                ROS_ERROR("[%s] Impossible to move to this position. (Width values: [0.0 - 110.0] ",  m_log_name.c_str());
                res.error = 255;
            }
        }
        return false;
    }

    std::tuple<bool, uint> Wsg50RosSrvs::moveWidthSpeed(float &width, float &speed) {

        bool legal{true};
        std::tie(legal, width, speed) = adjustGripperValues(width, speed);
        if (legal) {
            ROS_DEBUG("[%s] Moving to %.2f [mm] opening width at %.2f mm/s.", m_log_name.c_str(), width, speed);
            if (m_hw_api->graspCmd(width, speed)) {
                ROS_DEBUG("[%s]Target position reached.", m_log_name.c_str());
                return std::make_tuple(true, 0);
            }
        } else {
            ROS_ERROR("[%s] Impossible to move to this position. (Width values: [0.0 - 110.0] ",  m_log_name.c_str());
            return std::make_tuple(false, 255);
        }
        return std::make_tuple(false, 0);
    }

    bool Wsg50RosSrvs::graspSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            bool out{false};
            std::tie(out, res.error) = moveWidthSpeed(req.width, req.speed);
            return out;
        }
        return false;
    }

    bool Wsg50RosSrvs::releaseSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            bool out{false};
            std::tie(out, res.error) = moveWidthSpeed(req.width, req.speed);
            return out;
        }
        return false;
    }

    bool Wsg50RosSrvs::stopSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            ROS_WARN("[%s]Stop Gripper!",  m_log_name.c_str());
            bool ret{m_hw_api->stopCmd()};
            ROS_DEBUG("[%s]Stopped", m_log_name.c_str());
            return ret;
        }
        return false;
    }

    bool Wsg50RosSrvs::incrementSrv(wsg_50_msgs::Incr::Request &req, wsg_50_msgs::Incr::Response &res) {
        if (preProcessSrv(m_hw_api)) {
            if (req.direction == "open") {
                if (!m_object_grasped) {
                    auto nextWidth = m_hw_api->getOpenWidth() + req.increment;
                    return m_hw_api->moveCmd(std::min(GRIPPER_MAX_OPEN, nextWidth), 20, true);
                } else {
                    ROS_DEBUG("[%s] Releasing object...", m_log_name.c_str());
                    if (m_hw_api->releaseCmd(GRIPPER_MAX_OPEN, 20)) {
                        m_object_grasped = false;
                        return true;
                    }
                    return false;
                }
            } else if (req.direction == "close") {
                if (!m_object_grasped) {
                    auto nextWidth = m_hw_api->getOpenWidth() + req.increment;
                    return m_hw_api->moveCmd(std::max(GRIPPER_MIN_OPEN, nextWidth), 20, true);
                }
            }
            return true;
        }
        return false;
    }


    // Wsg50Ros
    bool Wsg50Ros::init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh) {
        if (!assert_hw(hw, nh)){
            return false;
        }
        m_hw_api = hw;
        // Subscribers
        m_subs.push_back(nh.subscribe("goal_position", 5, &Wsg50Ros::position_cb, this));
        m_subs.push_back(nh.subscribe("goal_speed", 5, &Wsg50Ros::speed_cb, this));
        m_subs.push_back(nh.subscribe("set_dsa_read", 5, &Wsg50Ros::setDSAcb, this));

        // Services
        m_get_dsa_data_srv = nh.advertiseService("get_dsa_data", &Wsg50Ros::getDsaSrv, this);
        m_get_dsa_cop_srv = nh.advertiseService("get_dsa_cop", &Wsg50Ros::getDsaCopSrv, this);

        // status publisher
        m_pub_status = std::make_shared<realtime_tools::RealtimePublisher<wsg_50_msgs::StatusFull>>(nh, "status", 100);
        ROS_INFO("[%s] started ROS-gripper interface", m_log_name.c_str());
        return true;
    }


    void Wsg50Ros::position_cb(const wsg_50_msgs::Cmd::ConstPtr &msg) {
        if (m_hw_api->getOpenWidth() == msg->pos)
            return;
        m_hw_api->setWidthCmd(msg->pos, msg->speed);
        ROS_DEBUG("[%s] Received new move command to %.2f mm at %.2f mm/s", m_log_name.c_str(), msg->pos, msg->speed);
        if (m_asynchronous) {
            if (m_hw_api->stopCmd(true)) {
                if (!m_hw_api->moveCmd(msg->pos, msg->speed, false, true))
                    ROS_ERROR("[%s] Failed to send move command to %.2f mm at %.2f mm/s", m_log_name.c_str(),
                              msg->pos, msg->speed);
            }
        }
    }

    void Wsg50Ros::speed_cb(const std_msgs::Float32::ConstPtr &msg) {
        m_hw_api->setSpeedCmd(msg->data);
    }

    bool Wsg50Ros::getDsaSrv(wsg_50_msgs::GetDsa::Request &req, wsg_50_msgs::GetDsa::Response &res){
        ROS_DEBUG("[%s] received DSA request", m_log_name.c_str());
        m_hw_api->requestDSA();

        ros::Rate ms{1000.0};
        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 1; i <req.timeout; i++){
            ms.sleep();
            if (m_hw_api->dsaRequestIsProcessed()){
                auto dsa_fingers = m_hw_api->getDsaDataPtr();
                res.dsa_finger0 = asMsg(dsa_fingers.first);
                res.dsa_finger1 = asMsg(dsa_fingers.second);
                auto dt_mu_sec = std::chrono::duration_cast<std::chrono::microseconds>( std::chrono::high_resolution_clock::now() - start).count();
                res.process_time = (float(dt_mu_sec) / 1e3);
                return true;
            }
            ms.sleep();
        }
        return false;
    }

    bool Wsg50Ros::getDsaCopSrv(wsg_50_msgs::GetDsaCop::Request &req, wsg_50_msgs::GetDsaCop::Response &res){
        ROS_DEBUG("[%s] received DSA Center of Pressure request", m_log_name.c_str());
        m_hw_api->requestSparseDSA();
        ros::Rate ms{1000.0};
        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 1; i <req.timeout; i++){
            ms.sleep();
            if (m_hw_api->dsaRequestIsProcessed()){
		        res.dsa_cop0 = asCopMsg(m_hw_api, 0);
                res.dsa_cop1 = asCopMsg(m_hw_api, 1);
                 auto dt_mu_sec = std::chrono::duration_cast<std::chrono::microseconds>( std::chrono::high_resolution_clock::now() - start).count();
                res.process_time = (float(dt_mu_sec) / 1e3);
                return true;
            }
        }
        return false;
    }

    void Wsg50Ros::setDSAcb(const wsg_50_msgs::SetDsa::ConstPtr  &msg){
        m_hw_api->setDSA(msg->enable_dsa);
        m_hw_api->setDSASparsely(msg->cop_only);
        m_read_dsa = msg->enable_dsa;
        m_read_dsa_sparsely = msg->cop_only;
    }

    bool Wsg50Ros::update(const ros::Time &time, const ros::Duration &period, const std::string &gripper_name){
        m_hw_api->update();
        m_pub_status->msg_.status = wsg_50::getStatusMsg(m_hw_api);
        if (m_read_dsa){
            if (m_read_dsa_sparsely){
                m_pub_status->msg_.dsa_cop_finger_0.header.stamp =ros::Time::now();
                m_pub_status->msg_.dsa_cop_finger_0.header.frame_id =gripper_name + "dsa_f0";
                m_pub_status->msg_.dsa_cop_finger_0.cop = asCopMsg(m_hw_api, 0);
                m_pub_status->msg_.dsa_cop_finger_1.header.stamp =ros::Time::now();                
                m_pub_status->msg_.dsa_cop_finger_1.header.frame_id =gripper_name + "dsa_f1";                
                m_pub_status->msg_.dsa_cop_finger_1.cop = asCopMsg(m_hw_api, 1);
                m_pub_status->msg_.dsa_finger_0.dsa_values.clear();
                m_pub_status->msg_.dsa_finger_1.dsa_values.clear();
            } else{
                auto dsa_fingers = m_hw_api->getDsaDataPtr();
                m_pub_status->msg_.dsa_finger_0 = asMsg(dsa_fingers.first);
                m_pub_status->msg_.dsa_finger_1 = asMsg(dsa_fingers.second);
            }
        } else{
            if (!m_pub_status->msg_.dsa_finger_0.dsa_values.empty())
                m_pub_status->msg_.dsa_finger_0.dsa_values.clear();
            if (!m_pub_status->msg_.dsa_finger_1.dsa_values.empty())
                m_pub_status->msg_.dsa_finger_1.dsa_values.clear();
        }
        if (m_pub_status->trylock()) {
            m_pub_status->unlockAndPublish();
        }
        return true;
    }


}
