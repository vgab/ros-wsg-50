from typing import List, Optional

import numpy as np
import rospy
import spatialmath as sm

import tf2_ros
from geometry_msgs.msg import TwistStamped
from wsg_50_msgs.msg import StatusFull, Status

from wsg_50_hw.constants import CELL_SIZE
from wsg_50_hw.dsa_conversions import dsa_msg_to_array, get_dsa_cop, calc_pose_error
from wsg_50_hw.utils import fix_ns

__all__ = ["GripperStateHandle", "DsaGripperStateHandle", "AlignmentObserver"]


class GripperStateHandle:
    """Basic WSG 50 python handle to be used in arbitrary python projects
    """

    def __init__(self, wsg_node_name, full_status=True):
        wsg_node_name = fix_ns(wsg_node_name)
        self._finger_forces = np.zeros(2)
        self._motor_force = 0.0
        self._acc = 0.0
        self._speed = 0.0
        self._pos = 0.0
        self._pos_prv = 0.0

        if full_status:
            self._sub = rospy.Subscriber(
                f'{wsg_node_name}status', StatusFull, self._full_state_cb, queue_size=100)
        else:
            self._sub = rospy.Subscriber(
                f'{wsg_node_name}status', Status, self._state_cb, queue_size=100)

    def _state_cb(self, msg):
        # type: (Status) -> None
        self._pos_prv = self._pos
        self._acc = msg.acc
        self._speed = msg.speed
        self._pos = msg.width
        self._motor_force = msg.force
        self._finger_forces[0] = msg.force_finger0
        self._finger_forces[1] = msg.force_finger1

    def _full_state_cb(self, msg):
        # type: (StatusFull) -> None
        self._state_cb(msg.status)

    @property
    def width(self):
        return self._pos

    @property
    def forces(self):
        return np.r_[self._motor_force, self._finger_forces]

    def __str__(self):
        return f"WSG 50 status:\nwidth:\t\t{self.width * 1e3} [mm]"


class DsaGripperStateHandle(GripperStateHandle):
    """extended gripper state handle
    """
    dsa_shape = (14, 6)
    dsa_size_m = np.r_[47.6, 20.4] * 1e-3

    def __init__(self, wsg_node_name):
        super().__init__(wsg_node_name, full_status=True)
        self.dsa_sum = [0, 0]
        self._dsa_msr = [np.zeros(self.dsa_shape), np.zeros(self.dsa_shape)]
        self._cop_msg = [np.zeros(2), np.zeros(2)]
        self._edges = [None, None]  # type: List[Optional[np.ndarray]]
        self._last_cop = rospy.Time(0, 0)
        self._last_dsa_mat = rospy.Time(0, 0)
        self._T_F0_F1 = sm.SE3.Rx(180., "deg")
        self._T_F0_TCP = sm.SE3.Ry(-90, "deg")
        self._T_F0_F1.A[1, 3] = self.dsa_size_m[1]
        self._T_F0_TCP.A[1, 3] = 0.5 * self.dsa_size_m[1]
        self.neglect_y_error_in_free_space = True

    @property
    def dsa_msr(self):
        if self._last_cop > self._last_dsa_mat:
            mat = [np.zeros(self.dsa_shape), np.zeros(self.dsa_shape)]
            for i, edge in enumerate(self._edges):
                if edge is not None:
                    for j in range(0, edge.shape[1], 2):
                        mat[i][edge[0, j] - 1: edge[0, j + 1], edge[1, j] - 1] = 1
                mat[i] *= self.dsa_sum[i] / max(1, mat[i].sum())
            return mat
        return self._dsa_msr

    @property
    def T_F0_F1(self):
        r"""
        get finger transformation

        .. math::

            {}^{F_0}{\bf T}_{F_1} =
                \begin{bmatrix}
                   {\bf R}_{x}(\pi)  & \begin{bmatrix}0\\c_y\\w \end{bmatrix} \\
                   {\bf 0} & 1
                \end{bmatrix}

        where :math:`c_y` denotes the DSA-matrix width and :math:`w` the gripper width

        Returns:
            sm.SE3: transformation from F1 to F0
        """
        self._T_F0_F1.A[2, 3] = self.width
        return self._T_F0_F1

    @property
    def T_F0_TCP(self):
        r"""
        get finger transformation

        .. math::

            {}^{F_0}{\bf T}_{F_1} =
                \begin{bmatrix}
                   {\bf R}_{y}(-\frac{\pi}{2})  & 0.5 \begin{bmatrix}0\\c_y\\w \end{bmatrix} \\
                   {\bf 0} & 1
                \end{bmatrix}

        where :math:`c_y` denotes the DSA-matrix width and :math:`w` the gripper width

        Returns:
            sm.SE3: transformation from TCP to F0
        """
        self._T_F0_TCP.A[2, 3] = 0.5 * self.width
        return self._T_F0_TCP

    def _cop_helper(self, i):
        r"""Get Center of Pressure of finger `i`

        Args:
            i (int): Finger ID, either 0 or 1.

        Returns:
            np.ndarray: center of pressure :math:`{}^{F_i}{\bf r}_{\mathrm{CoP}}` in finger frame
        """
        assert 0 <= i <= 1, "only two fingers available"
        if self.dsa_sum[i] == 0.:
            return np.r_[0.5 * self.dsa_size_m, 0]
        if self._last_cop > self._last_dsa_mat:
            return np.r_[self._cop_msg[i] * 1e-3, 0]
        return np.r_[get_dsa_cop(self._dsa_msr[i]), 0]

    @property
    def cop_only(self):
        """
        Return only center of pressure data obtained from gripper

        Returns:
            bool: flag if handle is currently receiving center of pressure data
        """
        return self._last_cop > self._last_dsa_mat

    @property
    def F0_r_cop(self):
        """get Center of pressure of finger 1 in finger 1 reference frame"""
        return self._cop_helper(0)

    @property
    def F1_r_cop(self):
        """get Center of pressure of finger 1 in finger 1 reference frame"""
        return self._cop_helper(1)

    @property
    def TCP_r_cop(self):
        """vector to center of pressure center in TCP frame
        """
        return self.T_F0_TCP.inv() * self.F0_r_cop, self.T_F0_TCP.inv() * self.T_F0_F1 * self.F1_r_cop

    def _full_state_cb(self, msg: StatusFull) -> None:
        self._state_cb(msg.status)
        self._last_cop = max(msg.dsa_cop_finger_0.header.stamp,
                             msg.dsa_cop_finger_1.header.stamp)
        for i in range(2):
            cop_data = getattr(msg, f"dsa_cop_finger_{i}").cop
            dsa_data = getattr(msg, f"dsa_finger_{i}")
            if len(cop_data.edge_x) > 0:
                self._edges[i] = np.vstack(
                    (np.r_[cop_data.edge_x], np.r_[cop_data.edge_y]))
            else:
                self._edges[i] = None
            self._cop_msg[i] = np.r_[cop_data.x, cop_data.y]
            if len(dsa_data.dsa_values) > 0:
                self._dsa_msr[i] = dsa_msg_to_array(dsa_data)
                self.dsa_sum[i] = self._dsa_msr[i].sum()
                self._last_dsa_mat = rospy.get_rostime()
            else:
                self._dsa_msr[i].fill(0.)
                self.dsa_sum[i] = cop_data.sum

    def __str__(self):
        base_str = super().__str__()
        base_str += f"\nCoP F0:\t{self.F0_r_cop}"
        base_str += f"\nCoP F1:\t{self.F1_r_cop}"
        return base_str

    @property
    def pose_error(self):
        error = calc_pose_error(
            F0_r_cop=self.F0_r_cop, F1_r_cop=self.F1_r_cop, F0_sum=self.dsa_sum[0], F1_sum=self.dsa_sum[1],
            T_TCP_F0=self.T_F0_TCP.inv(), T_F0_F1=self.T_F0_F1, width=self.width)
        if np.all((self.dsa_msr[0] != 0) == (self.dsa_msr[1] != 0.0)) and \
                (np.linalg.norm((self.T_F0_F1 * self.F1_r_cop - self.F0_r_cop[:, None])[:2]) <= CELL_SIZE):
            return np.r_[error[:3], np.zeros(3)]
        elif not self.neglect_y_error_in_free_space and np.sum(self.dsa_msr[0]) > 0 or np.sum(self.dsa_msr[1]) > 0:
            error[1] = 0.0
        return error

    @property
    def alignment_error(self):
        return calc_pose_error(
            F0_r_cop=self.F0_r_cop, F1_r_cop=self.F1_r_cop, F0_sum=self.dsa_sum[0], F1_sum=self.dsa_sum[1],
            T_TCP_F0=self.T_F0_TCP.inv(), T_F0_F1=self.T_F0_F1, width=self.width)


class AlignmentObserver(object):
    """Alignment python handle

    Serves as a helper class to subscribe to the current alignment error estimation
    from another ROS-node of style ``dsa_error_streamer``.
    """

    def __init__(self, topic_name):
        self._err = np.zeros(6)
        self._last_msg = rospy.get_rostime()
        self._frame = None
        self._sub = rospy.Subscriber(topic_name, TwistStamped, self.err_cb)
        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)

    def err_cb(self, msg):
        # type: (TwistStamped) -> None
        """
        ROS callback to subscribe bare alignment error

        Args:
            msg (TwistStamped): current alignment error estimation. Usually in tool-frame
        """
        self._frame = msg.header.frame_id
        self._last_msg = msg.header.stamp
        self._err[0] = msg.twist.linear.x
        self._err[1] = msg.twist.linear.y
        self._err[2] = msg.twist.linear.z
        self._err[3] = msg.twist.angular.x
        self._err[4] = msg.twist.angular.y
        self._err[5] = msg.twist.angular.z

    def err(self, frame=None):
        """
        Get the alignment error in reference frame ``err``

        Note:
            This transformation does not do a default coordinate transformation but
            only rotates the error in the dedicated reference frame.

        Args:
            frame (str, optional): reference frame. Defaults to None.

        Raises:
            NotImplementedError: if another reference frame is selected

        Returns:
            np.ndarray: alignment error in reference frame ``frame``
        """
        if (self._last_msg - rospy.get_rostime()).to_sec() > 1.0:
            rospy.logwarn("[WSG 50 alignment observer] last message obtained %2.2f seconds ago")
        if frame is None or self._frame is None:
            return self._err
        else:
            try:
                import quaternion
            except ModuleNotFoundError:
                RuntimeWarning("could not import quaternion. Return bare error")
                return self._err
            try:
                trans = self._tf_buffer.lookup_transform(frame, self._frame, rospy.Time(0))
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                RuntimeError("could not transform %s to %s", frame, self._frame)
                return np.zeros(6)
            q_C_T = np.quaternion(trans.transform.rotation.w,
                                  trans.transform.rotation.x,
                                  trans.transform.rotation.y,
                                  trans.transform.rotation.z)
            return np.r_[(q_C_T * np.quaternion(0.0, *self._err[0:3]) * q_C_T.conjugate()).vec,
                         (q_C_T * np.quaternion(0.0, *self._err[3:6]) * q_C_T.conjugate()).vec]

    def S(self, threshold=0.1, frame=None):
        r"""Binary selection vector

        using a threshold, each element of the 6-dim vector is given as

        .. math::

            s_{i} \gets \begin{cases} 1 & \text{if } |\varepsilon_{i}| \geq \iota \\
                                      0 & \text{else }
                        \end{cases}

        where :math:`\iota` is ``threshold`` and :math:`\varepsilon` is the current alignment
        error as returned from :py:meth:`~err`.

        Args:
            threshold (float, optional): threshold parameter. Defaults to 0.1.
            frame (str, optional): reference frame. Defaults to None.

        Returns:
            np.ndarray: boolean vector (6 x 1)
        """
        return np.abs(self.err(frame)) > threshold
