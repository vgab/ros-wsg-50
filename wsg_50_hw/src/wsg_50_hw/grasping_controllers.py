#!/usr/bin/env python
"""
Grasping Controllers
===========================

This module provides the Velocity- and Force-based Grasping controllers
as presented in [GablerICRA22]_.


Required ROS-parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

    the parameters below are all expected to be within the namespace ``{ros_param_ns}``,
    except it says explicitly `~`

For the base class, i.e. :py:class:`~GraspingController`, these parameters are needed

================================== ==================================================== ==============
parameter name                     notes                                                default value
================================== ==================================================== ==============
``~command_topic``                  publisher topic name for Cartesian velocity command
``gripper_alignment_error_topic``  subscriber topic for current alignment error in SE3
``base_link_name``                 name of robot base link name                         "base_link"
``ee_link_name``                   name of robot end-effector link name                 "ee_link"
``tool_link_name``                 name of robot tcp-link name                          "wsg_50_tcp"
================================== ==================================================== ==============

For the derived velocity-based grasping strategy  class, i.e. :py:class:`~VelMpcGraspingController`,
the table extends to

================================== ==================================================== ==============
parameter name                     notes                                                default value
================================== ==================================================== ==============
``u_v_max``                        upper bound for command output in translation [m/s]  5e-3
``u_v_rot_max``                    upper bound for command output in rotation [rad/s]   1e-2
``scale_velocity``                 if True, scale velocity according to current adjoint True
================================== ==================================================== ==============


For the derived force-based grasping strategy  class, i.e. :py:class:`~ForceGraspingController`,
the table extends to

================================== ==================================================== ==============
parameter name                     notes                                                default value
================================== ==================================================== ==============
``integral_window_size``           integral window size for I-term of PI-controller
``ft_ext_topic_name``              subscriber topic name for external F/T-sensor data
``cmd_in_ee_frame``                flag for current control-frame (base- vs. ee-link)   True
================================== ==================================================== ==============
"""
from abc import ABC, abstractmethod
from typing import Optional

import numpy as np
import quaternion

import rospy
from geometry_msgs.msg import TwistStamped, WrenchStamped

from wsg_50_msgs.msg import StrategySelect
import wsg_50_hw.utils as utils
from wsg_50_hw.gripper_state_handle import AlignmentObserver

__all__ = ["VelMpcGraspingController", "ForceGraspingController", "ApplyCompliantGrasping"]


def linear_error_model(dt=0.1):
    try:
        import casadi
    except ModuleNotFoundError:
        RuntimeWarning(
            "Could not import 'casadi' for MPC->model. Quit model generation")
        return
    try:
        import do_mpc
    except ModuleNotFoundError:
        RuntimeWarning(
            "Could not import `do_mpc` module. Quit model generation")
        return
    model_type = 'discrete'
    model = do_mpc.model.Model(model_type)
    x = model.set_variable(var_type='_x', var_name='x', shape=(3, 1))
    u = model.set_variable(var_type='_u', var_name='u', shape=(3, 1))

    A = np.eye(3)
    B = np.eye(3) * dt
    x_next = A @ x + B @ u
    model.set_rhs('x', x_next)
    model.set_expression(expr_name='cost', expr=casadi.sum1((x) ** 2))
    model.setup()
    return model


def base_mpc(u_max=1e-2 * np.ones((3, 1)), dt=0.1, lb_x=None, ub_x=None, **kwargs):
    try:
        import do_mpc
    except ModuleNotFoundError:
        raise RuntimeError("Could not import `do_mpc` module. Quit MPC setup")

    model = linear_error_model(dt)
    mpc = do_mpc.controller.MPC(model)
    setup_mpc = {
        'n_robust': 0,
        'n_horizon': 5,
        't_step': 0.05,
        'state_discretization': 'discrete',
        'store_full_solution': False,
    }
    setup_mpc.update(kwargs)
    mpc.set_param(**setup_mpc)
    mpc.set_rterm(u=1e-4)  # input penalty

    if lb_x is not None:
        if lb_x.shape != (3, 1):
            lb_x = np.reshape(lb_x, (3, 1))
        mpc.bounds['lower', '_x', 'x'] = lb_x

    if ub_x is not None:
        if ub_x.shape != (3, 1):
            ub_x = np.reshape(ub_x, (3, 1))

        mpc.bounds['upper', '_x', 'x'] = ub_x

    mpc.bounds['lower', '_u', 'u'] = -u_max
    mpc.bounds['upper', '_u', 'u'] = u_max

    mterm = model.aux['cost']
    lterm = model.aux['cost']
    mpc.set_objective(mterm=mterm, lterm=lterm)
    mpc.setup()
    return mpc


def init_mpc_helper(mpc, x0):
    mpc.x0 = x0
    mpc.set_initial_guess()


class GraspingController(ABC):

    def __init__(self,
                 error_topic: str, loop_hz: float, command_topic: str = None,
                 base_link="base_link", ee_link="tcp_controller", tool_link="wsg_50_tcp",
                 control_frame: Optional[str] = None,
                 err_pos_threshold=0.0, err_rot_threshold=0.0) -> None:
        self._base_link = base_link
        self._ee_link = ee_link
        self._tool_link = tool_link
        try:
            self._tf = utils.TfHandler(add=False)
        except IndexError:
            self._tf = utils.TfHandler(add=True)
        self.control_frame = tool_link if control_frame is None else control_frame
        self.err_threshold = np.r_[err_pos_threshold * np.ones(3),
                                   err_rot_threshold * np.ones(3)]
        self.error_handle = AlignmentObserver(error_topic)
        self.cmd = None
        if command_topic:
            self.cmd = rospy.Publisher(command_topic, TwistStamped, queue_size=50)
        self.rate = rospy.Rate(loop_hz)

    @classmethod
    def from_ros(cls, ros_param_ns, **kwargs):
        r"""Failsafe initialization method.
        Initialize class via

        .. code-block::python

            X = MyCustomClass.from_ros(...)

        that calls then the :py:meth:`~_from_ros` constructor, which is expected to call both
        ```__init__```, i.e. default constructor, and :py:meth:`~init_ros`, which initializes
        the ROS-API for the current class.

        In case a parameter is missing, the error is returned to the console / logging module.

        Args:
            ros_param_ns(str): prefix for loading ROS-parameters.
        """
        try:
            data = cls.get_ros_params(ros_param_ns=utils.fix_prefix(ros_param_ns))
            data.update(kwargs)
            return cls(**data)
        except KeyError as e:
            rospy.logwarn(
                f"could not initialize handle due @ {rospy.get_name()} to missing ROS-parameter {e}")

    def update(self):
        try:
            self.cmd.publish(utils.np2twist_stamped(self.u_cmd, self.control_frame))
        except AttributeError:
            pass
        self.rate.sleep()

    @staticmethod
    def get_ros_params(ros_param_ns) -> dict:
        """
        return all ROS-parameters for current class instance, as called from :py:meth:`~_from_ros`

        Args:
            ros_param_ns(str): prefix for current ROS-parameters.

        Returns:
            dict: ros-parameter in the form of parameter-name -> value.

        Note:
            in contrast to :py:meth:`~from_ros` this prefix is not corrected via :py:func:`~fix_prefix`.

        Raises:
            KeyError: in case a ROS-parameter is neither set nor provided with a default value.
        """
        ros_param_ns = utils.fix_prefix(ros_param_ns)
        return dict(error_topic=rospy.get_param(f"{ros_param_ns}gripper_alignment_error_topic"),
                    command_topic=rospy.get_param(f"~command_topic", "u_cmd"),
                    loop_hz=rospy.get_param(f"{ros_param_ns}gripper_alignment_ctrl_loop_hz"),
                    base_link=rospy.get_param(f"{ros_param_ns}base_link_name", "base_link"),
                    ee_link=rospy.get_param(f"{ros_param_ns}ee_link_name", "ee_link"),
                    tool_link=rospy.get_param(f"{ros_param_ns}tool_link_name", "wsg_50_tcp"))

    @property
    def err_cur(self) -> np.ndarray:
        """current alignment error in ``tool`` frame"""
        return self.error_handle.err()

    @property
    def quat_B_T(self):
        """get quaternion for rotation from tool to body frame"""
        return self._tf.T_A_B(A=self._base_link, B=self._tool_link)[1]

    @property
    def R_B_T(self):
        """get rotation matrix for rotation from tool to body frame"""
        return quaternion.as_rotation_matrix(self.quat_B_T)

    @property
    def base_err_translation(self):
        """get current translation error in base frame of robot"""
        return self._tf.A_vector(frame_A=self._base_link, frame_B=self._tool_link, B_vector=self.err_cur[0:3])

    @property
    def base_err_rotation(self):
        """get current rotation error in base frame of robot. Note: this only rotates the error"""
        return self._tf.A_vector(frame_A=self._base_link, frame_B=self._tool_link, B_vector=self.err_cur[3:6])

    @property
    def _S_diag(self) -> np.ndarray:
        """diagonal selection matrix for current strategy"""
        return np.array(np.abs(self.error_handle.err()) > self.err_threshold)

    @staticmethod
    def _rotated_S(S_diag, R_B_T):
        """rotate selection matrix to base frame"""
        S1 = np.zeros((3, 3))
        S2 = np.zeros((3, 3))
        np.fill_diagonal(S1, S_diag[0:3])
        np.fill_diagonal(S2, S_diag[3:6])
        return np.block([[R_B_T @ S1 @ R_B_T.T, np.zeros((3, 3))],
                         [np.zeros((3, 3)), R_B_T @ S2 @ R_B_T.T]])

    @property
    def S_R(self):
        """rotated selection matrix in base frame of robot"""
        if np.all(self._S_diag == 0.0):
            return np.zeros((6, 6))
        return self._rotated_S(self._S_diag, self.R_B_T)

    @property
    @abstractmethod
    def u_cmd(self):
        """return current command as 6-dim array"""


class VelMpcGraspingController(GraspingController):

    def __init__(self, v_rot_max, v_max, *_, scale_velocity=True, **kw) -> None:
        super().__init__(*_, **kw)
        dt = self.rate.sleep_dur.to_sec()
        self._mpc_t = base_mpc(dt=dt, u_max=v_max, **kw)
        self._mpc_r = base_mpc(dt=dt, u_max=v_rot_max, **kw)
        self._err_prev = np.zeros(6)
        self._err_model = np.zeros(6)
        self._scale_err = np.ones(6)
        if scale_velocity:
            A = self._tf.T_A_B_adjoint(A=self._ee_link, B=self._tool_link)
            tmp = np.linalg.norm(A @ np.r_[0., 0., 0., 1, 0., 0.], ord=1)
            self._scale_err[3] = 1.0 / tmp if tmp >= 1.0 else 1.0
            tmp = np.linalg.norm(A @ np.r_[0., 0., 0., 0., 1., 0.], ord=1)
            self._scale_err[4] = 1.0 / tmp if tmp >= 1.0 else 1.0
            tmp = np.linalg.norm(A @ np.r_[0., 0., 0., 0., 0., 1.0], ord=1)
            self._scale_err[5] = 1.0 / tmp if tmp >= 1.0 else 1.0
        rospy.sleep(0.1)
        x0 = self.err_cur
        init_mpc_helper(self._mpc_t, x0[0:3])
        init_mpc_helper(self._mpc_r, x0[3:6])

    @staticmethod
    def get_ros_params(ros_param_ns) -> dict:
        ros_param_ns = utils.fix_prefix(ros_param_ns)
        params = GraspingController.get_ros_params(ros_param_ns)
        params.update(dict(
            v_max=rospy.get_param(f"{ros_param_ns}u_v_max", 5e-3),
            v_rot_max=rospy.get_param(f"{ros_param_ns}u_v_rot_max", 1e-2),
            scale_velocity=rospy.get_param(f"{ros_param_ns}scale_velocity", True)
        ))
        return params

    @property
    def err_cur(self) -> np.ndarray:
        return self._scale_err * super(VelMpcGraspingController, self).err_cur

    @property
    def u_cmd(self) -> np.ndarray:
        x0 = self.err_cur
        return np.r_[self._mpc_t.make_step(x0[0:3]).flatten(),
                     self._mpc_r.make_step(x0[3:6]).flatten()]


class ForceGraspingController(GraspingController):

    def __init__(self, N_i: int, ft_topic_name: str, *_,
                 in_ee_frame=False, **__) -> None:
        super().__init__(*_, **__)
        self.control_frame = self._ee_link if in_ee_frame else self._base_link
        self._K_F_p = np.zeros((6, 6))
        np.fill_diagonal(self._K_F_p, [8e-4, 8e-4, 8e-4, 2e-3, 2e-3, 2e-3])
        self._K_F_i = np.zeros((6, 6))
        np.fill_diagonal(self._K_F_i, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.B_F_des = np.zeros(6)
        self._err_ft_window = np.zeros((N_i, 6))
        self._R_B_T_window = np.zeros((N_i, 3, 3))
        self._selection_window = np.zeros((N_i, 6))
        self._ft_frame = ""
        self._ft_ext_subs = rospy.Subscriber(ft_topic_name, WrenchStamped, self._ft_cb)
        self._ft_ext = np.zeros(6)

    @staticmethod
    def get_ros_params(ros_param_ns) -> dict:
        ros_param_ns = utils.fix_prefix(ros_param_ns)
        params = GraspingController.get_ros_params(ros_param_ns)
        params.update(dict(
            N_i=rospy.get_param(f"{ros_param_ns}integral_window_size"),
            ft_topic_name=rospy.get_param(f"{ros_param_ns}ft_ext_topic_name"),
            in_ee_frame=rospy.get_param(f"{ros_param_ns}cmd_in_ee_frame", True)

        ))
        return params

    @property
    def B_F_ext(self) -> np.ndarray:
        if self._ft_frame == "" or self._ft_frame == self.control_frame:
            return self._ft_ext
        return np.hstack((self._tf.A_vectors(self._ft_ext[0:3], self._ft_ext[3:6],
                                             frame_A=self._base_link, frame_B=self._ft_frame)))

    def _ft_cb(self, msg: WrenchStamped) -> None:
        self._ft_frame = msg.header.frame_id
        self._ft_ext[0:3] = utils.vec32np(msg.wrench.force)
        self._ft_ext[3:6] = utils.vec32np(msg.wrench.torque)

    @property
    def u_I(self):
        """get integral term and adjust rolling buffers"""

        def get_integral_part(err, R, S):
            return np.sum(
                np.einsum('nij, ni, nji -> ni', R, S, R) * err,
                axis=0)

        u_I = self._K_F_i @ np.r_[
            get_integral_part(
                self._err_ft_window[:, 0:3], self._R_B_T_window, self._selection_window[:, 0:3]),
            get_integral_part(
                self._err_ft_window[:, 3:6], self._R_B_T_window, self._selection_window[:, 3:6])
        ]
        self._R_B_T_window = np.roll(self._R_B_T_window, 1, 0)
        self._R_B_T_window[0, :, :] = self.R_B_T.copy()
        self._selection_window = np.roll(self._selection_window, 1, 0)
        self._selection_window[0, :] = self._S_diag.copy()
        self._err_ft_window = np.roll(self._err_ft_window, 1, 0)
        self._err_ft_window[0, :] = self.B_err_F.copy()
        self._selection_window[1:, np.where(
            self._selection_window[0, :] != self._selection_window[1, :])] = 0.0
        return u_I

    @property
    def B_err_F(self) -> np.ndarray:
        return self.B_F_des - self.B_F_ext

    @property
    def B_u_cmd(self) -> np.ndarray:
        return self.S_R @ self._K_F_p @ self.B_err_F + self.u_I

    @property
    def u_cmd(self):
        if self.control_frame == self._base_link:
            return self.B_u_cmd
        B_u_cmd = self.B_u_cmd
        return np.hstack(self._tf.A_vectors(B_u_cmd[0:3], B_u_cmd[3:6],
                                            frame_A=self.control_frame, frame_B=self._base_link))


class ApplyCompliantGrasping(GraspingController):
    F_CTRL = -1
    V_CTRL = 1

    def __init__(self, u_vel_topic_name, u_ft_topic_name, strategy_topic_name,
                 **kwargs) -> None:
        super().__init__(**kwargs)
        self._tf = utils.TfHandler(add=False)
        self._B_u_E_vel_cmd = np.zeros(6)
        self._B_u_E_force_cmd = np.zeros(6)
        self._strategies = np.zeros(6)
        self._subs = []
        if u_vel_topic_name is None and u_ft_topic_name:
            rospy.logerr("did not receive any input topic names. Exit")
            return
        if u_vel_topic_name:
            self._subs.append(rospy.Subscriber(u_vel_topic_name, TwistStamped,
                                               self._u_vel_cb, queue_size=30))
        else:
            rospy.logerr("did not find a topic for velocity-based commands")
        if u_ft_topic_name:
            self._subs.append(rospy.Subscriber(u_ft_topic_name, TwistStamped,
                                               self._u_force_cb, queue_size=30))
        else:
            rospy.logerr("did not find a topic for force-based commands")

        if strategy_topic_name is not None:
            self._subs.append(rospy.Subscriber(strategy_topic_name,
                                               StrategySelect,
                                               self._strategy_cb))

    @staticmethod
    def get_ros_params(ros_param_ns) -> dict:
        ros_param_ns = utils.fix_prefix(ros_param_ns)
        params = GraspingController.get_ros_params(ros_param_ns)
        params['command_topic'] = None
        params.update(dict(u_vel_topic_name=utils.get_param(f"{ros_param_ns}velocity_based_command_topic_name", ""),
                           u_ft_topic_name=utils.get_param(f"{ros_param_ns}force_based_command_topic_name"),
                           strategy_topic_name=utils.get_param(f"{ros_param_ns}strategy_selection_topic_name")))
        return params

    @property
    def strategies(self):
        return self._strategies

    def set_strategies(self, S_F: np.ndarray, S_v: np.ndarray):
        self._strategies.fill(0.0)
        if np.any(np.logical_and(S_v, S_F)):
            rospy.logerr_throttle(1.0, "multiple strategies along one axis are not allowed. Ignore current data")
            return
        self._strategies[S_v] = self.V_CTRL
        self._strategies[S_F] = self.F_CTRL
        rospy.logdebug_throttle(0.2, f"new strategy-vector set to {self.strategies}")

    def _strategy_cb(self, msg):
        """
        Adjust current strategy from ROS-message

        Args:
            msg(HybridForceVelocityControlSelect): selection message
        """

        def bool2arr(bl_list):
            S = np.array(bl_list, dtype=bool)
            if S.shape[0] < self.strategies.shape[0]:
                return np.pad(S, (0, self.strategies.shape[0] - S.shape[0]))
            return S

        self.set_strategies(S_v=bool2arr(msg.S_vel), S_F=bool2arr(msg.S_force))

    def _u_vel_cb(self, msg: TwistStamped):
        try:
            import spatialmath as sm
        except ModuleNotFoundError:
            rospy.logerr_once("the velocity based strategy needs the spatialmath-python module")
            return
        C_u_C_cmd = np.hstack((utils.vec32np(msg.twist.linear),
                               utils.vec32np(msg.twist.angular)))
        p_C_EC, q_E_C = self._tf.T_A_B(A=self._ee_link, B=msg.header.frame_id)
        T_vel_E_C = sm.base.tr2adjoint(np.block([
            [quaternion.as_rotation_matrix(q_E_C), p_C_EC[:, None]],
            [np.zeros(3), 1.0]
        ]))
        C_u_C_cmd[self.strategies != self.V_CTRL] = 0.0
        E_u_E_cmd = T_vel_E_C @ C_u_C_cmd
        self._B_u_E_vel_cmd[0:3], self._B_u_E_vel_cmd[3:6] = \
            self._tf.A_vectors(E_u_E_cmd[0:3], E_u_E_cmd[3:6],
                               frame_A=self._base_link, frame_B=self._ee_link)

    def _u_force_cb(self, msg: TwistStamped):
        def get_S_R_tilde(S_diag):
            S = np.zeros((3, 3))
            np.fill_diagonal(S, S_diag)
            return self.R_B_T @ S @ self.R_B_T.T

        try:
            assert msg.header.frame_id != ""
            B_u_pos, B_u_rot = self._tf.A_vectors(utils.vec32np(msg.twist.linear),
                                                  utils.vec32np(msg.twist.angular),
                                                  frame_A=self._base_link, frame_B=msg.header.frame_id,
                                                  time=msg.header.stamp)
            self._B_u_E_force_cmd[0:3] = get_S_R_tilde(self.strategies[0:3] == self.F_CTRL) @ B_u_pos
            self._B_u_E_force_cmd[3:6] = get_S_R_tilde(self.strategies[3:6] == self.F_CTRL) @ B_u_rot
        except AssertionError:
            rospy.logerr_once("no reference frame provided. I will ignore inputs unless adjusted!")

    @property
    def B_u_cmd(self):
        return self._B_u_E_force_cmd + self._B_u_E_vel_cmd

    def apply_via_cobot_handle(self, cobot, **cmd_kwargs):
        r"""
        Apply control command via the :py:meth:`update` function

        Args:
            cobot: interface to Cartesian velocity control of a robot
            **cmd_kwargs: additional arguments for ``update``
        """
        cobot.update(u_cmd=self.B_u_cmd,
                     u_cmd_frame=self._base_link, **cmd_kwargs)

    @property
    def u_cmd(self):
        if self.control_frame == self._base_link:
            return self.B_u_cmd
        B_u_cmd = self.B_u_cmd
        return np.hstack(self._tf.A_vectors(B_u_cmd[0:3], B_u_cmd[3:6],
                                            frame_A=self.control_frame, frame_B=self._base_link))
