import numpy as np
import spatialmath as sm
from typing import Tuple

from wsg_50_msgs.msg import DsaData
from wsg_50_msgs.srv import GetDsaRequest

from .constants import *

__all__ = ["get_dsa_matrices", "dsa_msg_raw_to_array", "dsa_msg_to_array", "get_dsa_cop", "calc_pose_error"]


def dsa_msg_raw_to_array(dsa_msg):
    """Parse DSA data

    Args:
        dsa_msg(DsaData): wsg_50_msgs/DsaData python-object

    Returns:
        np.ndarray: converted data matrix (not flipped)
    """
    return np.reshape(np.array(dsa_msg.dsa_values), (dsa_msg.height, dsa_msg.width))


def dsa_msg_to_array(dsa_msg, flip=False) -> np.ndarray:
    """
    parse ros-message to array:

    #. reshape to matrix
    #. (optionally) flip index order as internal sensor enumeration is in reverse order than expected

    Args:
        dsa_msg(DsaData): wsg_50_msgs/DsaData python-object
        flip(bool,optional): flip DSA matrix, e.g. for plotting

    Returns:
        np.ndarray: converted data matrix
    """
    arr = dsa_msg_raw_to_array(dsa_msg)
    if flip:
        return np.flip(arr)
    return arr


def get_dsa_matrices(srv, timeout=10000):
    """
    Get  DSA matrices via rospy service and transforms each DSA message from a rospy service response to the
    dedicated numpy array.
    Note that as outlined in :py:func:`~dsa_msg_to_array` the matrices are not only reshaped into the original matrix
    but also flipped to reverse the original order.

    Args:
        srv(rospy.ServiceProxy): Service Proxy of type GetDsa
        timeout(int): timeout for service call. Defaults to 10000

    Returns:
        Tuple[np.ndarray, np.ndarray]: both DSA matrices of both fingers
    """
    try:
        dsa_data = srv(GetDsaRequest(timeout=timeout))
        return dsa_msg_to_array(dsa_data.dsa_finger0), dsa_msg_to_array(dsa_data.dsa_finger1)
    except:
        np.zeros((14, 6)), np.zeros((14, 6))


def get_dsa_cell_centers():
    """Generate DSA position vectors for each cell as a matrix where each element contains a 2d-position vector
    denoting the displacement from center to the current cell in the frame of the current finger.

    Returns:
        np.ndarray: cell center array in finger frame (14 x 6 x 2)
    """
    cell_centers = np.zeros((14, 6, 2))
    for i in range(14):
        for j in range(6):
            cell_centers[i, j] = CELL_SIZE * (i + 0.5), CELL_SIZE * (j + 0.5)
    return cell_centers


CELL_CENTERS = get_dsa_cell_centers()


def get_dsa_cop(dsa_mat: np.ndarray, cell_centers=None) -> np.ndarray:
    """
    Sensor dimensions:
        total array size: 47.6 x 20.4 mm
        cell size (quadratic): 3.4 mm

    see `CELL_SIZE` and `CELL_CENTERS`, i.e. :py:func:`~get_dsa_cell_centers` for insights.

    Args:
        dsa_mat (np.ndarray): 2D binary sensor readings from 0-4096

    Returns:
        np.ndarray: center of presure
    """
    if cell_centers is None:
        cell_centers = CELL_CENTERS
    total = dsa_mat.sum()
    if total > 0.:
        return np.array([(cell_centers[:, :, 0] * dsa_mat).sum() / total,
                         (cell_centers[:, :, 1] * dsa_mat).sum() / total])
    return np.r_[SENSOR_H / 2.0, SENSOR_W / 2.0]


def calc_pose_error(F0_r_cop, F1_r_cop, F0_sum, F1_sum,
                    T_F0_F1, T_TCP_F0, width,
                    TCP_r_cop0=np.array(TCP_r_COP0), TCP_r_cop_connect_des=np.r_[1.0, 0., 0.],
                    iota=1e-4) -> np.ndarray:
    r"""
    Calculate gripper pose error from obtained center of pressure vectors and current CS-transformations.

    Note:

        referring to the gripper visualization in the web-interface, the

         * right finger is denoted as 0
         * left finger is denoted as 1

    - Get vector from :math:`{}^{F_0}{\bf r}_{\mathrm{CoP}}` to :math:`{}^{F_1}{\bf r}_{\mathrm{CoP}}`

    Position:

    #. Get center point on vector connecting both CoPs
    #. transform center into TCP-frame
    #. return pos-error as displacement from zero-pos


    Attitude:

    #. normalize to unit-vector and rotate to `TCP` frame
    #. obtain yaw-error over tan-relation of :math:`{}^{TCP}{\bf e}_{x}` and :math:`{}^{TCP}{\bf e}_{y}`
    #. obtain pitch-error over sin of :math:`{}^{TCP}{\bf e}_{z}` and (unit-)vector length

    Note:
        due to the symmetry of the unit-vector, the roll error is neglected from the CoP calculations

    Args:
        F0_r_cop  (np.ndarray): Center of pressure of finger 0 in finger 0 frame
        F1_r_cop  (np.ndarray): Center of pressure of finger 1 in finger 1 frame
        T_F0_F1   (sm.SE3): Transformation from finger 1 to finger 0
        T_TCP_F0  (sm.SE3): Transformation from finger 0 to TCP
        width (float): current gripper opening width
        F0_sum (int): DSA-sum in finger 0
        F1_sum (int): DSA-sum in finger 1
        TCP_r_cop0(np.ndarray): Center of pressure in TCP if no pose error is encountered

    Returns:
        np.ndarray: Cartesian pose error
    """

    def vectorize(x):
        if len(x.shape) == 1:
            return x[:, None]
        return x

    def unit_vec(x):
        return vectorize(x) / np.linalg.norm(x)

    def get_R_from_tcp_vec(tcp_unit_vec, support_vec=None):
        try:
            tcp_unit_vec = tcp_unit_vec.flatten() / np.linalg.norm(tcp_unit_vec)
            if support_vec is None:
                tmp = np.linalg.norm(tcp_unit_vec[1:])
                support_vec = np.r_[tmp, np.sqrt(1.0 - tmp), 0.0]
            support_vec /= np.linalg.norm(support_vec)
        except ZeroDivisionError:
            return np.eye(3)
        z = np.cross(tcp_unit_vec, support_vec)
        y = np.cross(z, tcp_unit_vec)
        return np.hstack(list(map(unit_vec, (tcp_unit_vec, y, z))))

    err = np.zeros((6, 1))
    if F0_sum > 0 and F1_sum > 0:
        F0_r_cop_connect = T_F0_F1 * F1_r_cop - vectorize(F0_r_cop[:, None])
        # pos error
        TCP_r_copCenter = T_TCP_F0 * (vectorize(F0_r_cop) + 0.5 * F0_r_cop_connect)
        err[0:3] = TCP_r_copCenter - vectorize(TCP_r_cop0)
        # att error
        R_err = get_R_from_tcp_vec(T_TCP_F0.R @ F0_r_cop_connect) @ get_R_from_tcp_vec(TCP_r_cop_connect_des).T
        err[3:6, 0] = sm.base.tr2rpy(R_err)
    elif F0_sum > 0:
        err[0:3] = T_TCP_F0 * np.r_[[0., (SENSOR_W / 2.0 - F0_r_cop[1]) / (0.5 * SENSOR_W), 1]] * 0.5 * width
    elif F1_sum > 0:
        err[0:3] = T_TCP_F0 * T_F0_F1 * np.r_[[0., (SENSOR_W - 2.0 - F1_r_cop[1]) / (0.5 * SENSOR_W), 1]] * 0.5 * width
    err[np.abs(err) < iota] *= 0.0
    return err.flatten()


def arrays_to_pose_error(dsa_f0: np.ndarray, dsa_f1: np.ndarray,
                         T_F0_F1: sm.SE3, T_TCP_F0: sm.SE3, width: float,
                         TCP_r_cop0: np.ndarray = np.zeros(3)) -> np.ndarray:
    """
    Get pose error from two DSA-arrays and current CS-transformations.

    Args:
        dsa_f0 (np.ndarray): DSA array measurement in finger 0
        dsa_f1 (np.ndarray): DSA array measurement in finger 1
        T_F0_F1   (sm.SE3): Transformation from finger 1 to finger 0
        T_TCP_F0  (sm.SE3): Transformation from finger 0 to TCP
        width (float): gripper opening width
        TCP_r_cop0(np.ndarray, optional): Center of presure in TCP if pose is correct. Defaults to np.zeros(3), i.e. the TCP origin

    Returns:
        np.ndarray: Cartesian pose error
    """
    return calc_pose_error(F0_r_cop=np.r_[get_dsa_cop(dsa_f0), 0], F1_r_cop=np.r_[get_dsa_cop(dsa_f1), 0],
                           F0_sum=dsa_f0.sum(), F1_sum=dsa_f1.sum(),
                           T_F0_F1=T_F0_F1, T_TCP_F0=T_TCP_F0, width=width,
                           TCP_r_cop0=np.array(TCP_r_COP0))
