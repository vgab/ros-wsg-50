#!/usr/bin/env python
import matplotlib.pylab as plt
import numpy as np

from .constants import *
from .dsa_conversions import get_dsa_cop
from .gripper_state_handle import DsaGripperStateHandle

__all__ = ["animate_dsa_tcp", "print_error", "plot_tcp", "animate_dsa", "init_figure"]


def init_figure():
    """Initialize figure for DSA visualization

    Returns:
        figure handle and axes for both finger arrays
    """
    fig, axs = plt.subplots(1, 2)
    for ax in axs:
        plot_tcp(ax, CENTER)
    fig.legend(handles=axs.item(0).get_lines(), loc='lower center', bbox_to_anchor=(0, 0, 1, 1), ncol=2)
    plt.subplots_adjust(bottom=0.25, top=0.85)
    return fig, np.r_[axs[1], axs[0]]


def plot_dsa_array(ax, f_id, dsa_mat):
    ax.clear()
    ax.set_title(f'Finger F{f_id} ({"left" if f_id == 1 else "right"})')
    ax.set_xlabel(f'${{}}^{{F_{{{f_id}}}}}x$')
    ax.set_ylabel(f'${{}}^{{F_{{{f_id}}}}}y$', rotation=0)
    ax.imshow(dsa_mat, interpolation='nearest',
              extent=[0, SENSOR_AREA_MM[1], SENSOR_AREA_MM[0], 0],
              vmin=SENSOR_RANGE[0], vmax=SENSOR_RANGE[1],
              label='Sensor Data')


def plot_tcp(ax, cop, center=CENTER):
    ax.plot(center[1], center[0], 'go', markersize=10, label='CoP desired')
    ax.plot(cop[1], cop[0], 'ro', markersize=7, label='CoP (live)')


def plot_yaw_error(fig, yaw, yaw_tol=1.0):
    """Optionally plot Yaw Error in plotting interface

    Args:
        fig (): figure handle
        yaw (float): yaw angle in DEG
        yaw_tol (float, optional): yaw tolerance in DEG to change color to acceptable / green. Defaults to 1.0.
    """
    direction = r'$\Rightarrow$' if np.sign(yaw) < 0 else r'$\Leftarrow$'
    fig.text(0.5, 0.95, f'{direction} CoP Missalignment = ${yaw:+.3f}\\degree$ {direction} ', fontsize=14,
             horizontalalignment='center',
             bbox=dict(boxstyle="round", color=('lightgreen' if np.abs(yaw) < yaw_tol else 'red')))


def print_error(fig, pose_error, txt_handle=None):
    """Optionally print pose Error in plotting interface

    Args:
        fig (): figure handle
    """
    if np.linalg.norm(pose_error) < 1e-2:
        err_str = r"${\bf \varepsilon = 0}$"
        color = 'lightgreen'
    else:
        ang_err = np.rad2deg(pose_error[3:])
        mm_err_trans = pose_error[:3] * 1e3
        err_str = f"${{\\bf \\varepsilon [mm, deg] }}= [{mm_err_trans[0]:.2f}, {mm_err_trans[1]:.2f}, {mm_err_trans[2]:.2f},"
        err_str += f"{ang_err[0]:.2f}, {ang_err[1]:.2f}, {ang_err[2]:.2f}]$"
        color = 'red'
    if txt_handle is None:
        txt_handle = fig.text(0.5, 0.15, err_str, fontsize=14, horizontalalignment='center')
    else:
        txt_handle.set_text(err_str)
    txt_handle.set_bbox(dict(boxstyle="round", color=color))
    return txt_handle


def _update_error_text(fig, gripper, error_txt_handle=None):
    if error_txt_handle is not None:
        print_error(fig, gripper.pose_error, error_txt_handle)


def _update_title(fig, gripper, title_str=None):
    if title_str is not None:
        fig.suptitle(f"{title_str} {'(CoP-only)' if gripper.cop_only else '(DSA-Array reading)'}")


def animate_dsa(_, fig, axs, gripper, error_txt_handle=None, title_str=None):
    for i in range(2):
        axs[i].clear()
        plot_dsa_array(axs[i], i, gripper.dsa_msr[i])
        if i == 1:
            plot_tcp(axs[i], gripper.F1_r_cop * 1e3)
        else:
            plot_tcp(axs[i], gripper.F0_r_cop * 1e3)
    _update_error_text(fig, gripper, error_txt_handle=error_txt_handle)
    _update_title(fig, gripper, title_str=title_str)


def animate_dsa_tcp(_, fig, axs, gripper, error_txt_handle=None, title_str=None, TCP_r_cop0=np.array(TCP_r_COP0)):
    """Animate DSA data in TCP frame
    To be used with care as rotation is hardcoded in this function
    """
    for i in range(2):
        axs[i].clear()
        if i == 1:
            title = 'F1 (left) ' + rf'${{}}^{{\mathfrak{{t}}}}x={gripper.width / 2.0 * 1e3:.2f}$ mm'
        else:
            title = 'F0 (right) ' + rf'${{}}^{{\mathfrak{{t}}}}x={-gripper.width / 2.0 * 1e3:.2f}$ mm'
        axs[i].set_title(title)
        axs[i].set_xlabel(r'${}^{\mathfrak{t}}y$')
        axs[i].set_ylabel(r'${}^{\mathfrak{t}}z$', rotation=0)
    dsa_kargs = dict(extent=[-SENSOR_AREA_MM[1] / 2.0, SENSOR_AREA_MM[1] / 2.0, 0, -SENSOR_AREA_MM[0]],
                     vmin=SENSOR_RANGE[0], vmax=SENSOR_RANGE[1])
    axs[0].imshow(np.flip(gripper.dsa_msr[0], axis=0), interpolation='nearest', label='Sensor Data (F0/R)', **dsa_kargs)
    axs[1].imshow(np.flip(gripper.dsa_msr[1]), interpolation='nearest', label='Sensor Data (F1/L)', **dsa_kargs)
    for i, tcp_cop in enumerate(gripper.TCP_r_cop):
        plot_tcp(axs[i], np.r_[tcp_cop[2], tcp_cop[1]] * 1e3, np.r_[TCP_r_cop0[2], TCP_r_cop0[1]] * 1e3)
    _update_error_text(fig, gripper, error_txt_handle=error_txt_handle)
    _update_title(fig, gripper, title_str=title_str)
