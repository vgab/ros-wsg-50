import rospy

__all__ = ["fix_prefix", "fix_ns", "get_param"]


def fix_prefix(prefix):
    if prefix == "~":
        return prefix
    if prefix[-1] == "/":
        return prefix
    return f"{prefix}/"


def fix_ns(ns):
    if ns == "~":
        return ns
    if ns[0] != '/':
        ns = f"/{ns}"
    if ns[-1] != "/":
        ns = f"{ns}/"
    return ns


def get_param(param_name, default=None):
    """
    Get ROS-parameter function

    Args:
        param_name(str): ros-parameter name
        default(Any): default value for ROS-parameter lookup

    Returns:
        Any: value for current ROS-parameter name
    """
    if default is None:
        return rospy.get_param(param_name)
    try:
        return rospy.get_param(param_name)
    except KeyError:
        rospy.loginfo(f"[{rospy.get_name()}] "
                      f"could not get ROS-parameter {param_name.replace('~', f'{rospy.get_name()}/')}. "
                      f"Use default {default} instead.")
        return default
