import numpy as np
from wsg_50_msgs.msg import DsaData
from wsg_50_msgs.srv import GetDsaRequest


__all__ = ["get_dsa_matrices", "dsa_msg_to_array"]


def dsa_msg_to_array(dsa_msg):
    """
    parse ros-message to array:

    #. reshape to matrix
    #. flip index order as internal sensor enumeration is in reverse order than expected

    Args:
        dsa_msg(DsaData): wsg_50_msgs/DsaData python-object

    Returns:
        np.ndarray: converted data matrix
    """
    arr = np.reshape(np.array(dsa_msg.dsa_values),
                     (dsa_msg.height, dsa_msg.width))
    return np.flip(arr)


def get_dsa_matrices(srv, timeout=10000):
    """
    Get  DSA matrices via rospy service and transforms each DSA message from a rospy service reponse to the
    dedicated numpy array.
    Note that as outlined in :py:func:`~dsa_msg_to_array` the matrices are not only reshaped into the original matrix
    but also flipped to reverse the original order.

    Args:
        srv(rospy.ServiceProxy): Service Proxy of type GetDsa
        timeout(int): timeout for service call. Defaults to 10000

    Returns:
        np.ndarray, np.ndarray: both DSA matrices of both fingers
    """
    try:
        dsa_data = srv(GetDsaRequest(timeout=timeout))
        return dsa_msg_to_array(dsa_data.dsa_finger0), dsa_msg_to_array(dsa_data.dsa_finger1)
    except:
        np.zeros((14, 6)), np.zeros((14, 6))
