#include <signal.h>
#include <thread>

#include <wsg_50_hw/wsg_50_api.h>
#include <wsg_50_hw/wsg_50_ros.h>

using namespace wsg_50;



void sigint_handler(int sig) {
    ROS_INFO("Exiting...");

    ros::shutdown();
}

/**
 * The main function
 */

struct WsgNodeParams{
    std::string joint_state_topic{"joint_states"};
    std::string gripper_name{"wsg_50"};
    std::vector<std::string> joints{"wsg_50_gripper_base_joint_gripper_left",
                                    "wsg_50_gripper_base_joint_gripper_right"};
    std::string frame_id{"wsg_50_gripper_base_link"};

    double loop_hz{5.0};
    bool publish_joints{true};
    bool use_dsa_ros{false};
    bool use_dsa_sparsely{false};
    bool use_all_wsg_services{false};
    bool use_command_pub_ros_interface{true};

    WsgNodeParams(const std::string p_n=""){
        ros::NodeHandle nh{"~"};
        std::string base_param{p_n};
        nh.param(base_param + "joint_names", joints, joints);
        nh.param<std::string>(base_param + "frame_id", frame_id, frame_id);
        nh.param<std::string>(base_param + "joint_state_topic", joint_state_topic, joint_state_topic);
        nh.param<std::string>(base_param + "gripper_name", gripper_name, gripper_name);
        nh.param<double>(base_param + "loop_hz", loop_hz, loop_hz);
        nh.param<bool>(base_param + "publish_joints", publish_joints, publish_joints);
        nh.param<bool>(base_param + "use_dsa",use_dsa_ros, use_dsa_ros);
        nh.param<bool>(base_param + "use_dsa_sparsely",use_dsa_sparsely, use_dsa_sparsely);
        nh.param<bool>(base_param + "use_all_wsg_services", use_all_wsg_services, use_all_wsg_services);
    }

    inline std::string info() const{
        std::stringstream tmp;
        if (joints.size() >1){
            tmp << "\n\tleft joint:                   \t" << joints[0]
                << "\n\tright joint:                  \t" << joints[1];
        }
        tmp << "\n\tgripper_name:                 \t" << gripper_name
            << "\n\tframe_id:                     \t" << frame_id
            << "\n\tloop_hz:      [1/s]           \t" << loop_hz
            << "\n\tjoint state topic:            \t" << joint_state_topic
            << "\n\tpublish_joints:               \t" << publish_joints
            << "\n\tuse_dsa_ros:                  \t" << use_dsa_ros
            << "\n\tread sparse dsa:              \t" << use_dsa_sparsely
            << "\n\tuse_all_wsg_services:          \t" << use_all_wsg_services;
        return tmp.str();
    }
};

  


int main(int argc, char **argv) {
    ros::init(argc, argv, "wsg_50_hw");
    ros::AsyncSpinner spinner(4);
    spinner.start();
    ros::NodeHandle nh("~");    
    signal(SIGINT, sigint_handler);
    std::shared_ptr<Wsg50API> gripper_if = std::make_shared<Wsg50API>();        
    WsgNodeParams params{}; 
    ros::Rate rate(params.loop_hz);
    Wsg50Ros ros_if{};
    std::shared_ptr<Wsg50RosSrvs> full_srvs{nullptr};
    std::shared_ptr<Wsg50RosBaseSrvs> base_srvs{nullptr};    
    sensor_msgs::JointState joint_states;
    std::shared_ptr<ros::Publisher> pub_joints{};
    
    // initialilze gripper driver
    if (!gripper_if->connect(nh)){
        ROS_ERROR("[%s] Failed to connect to gripper. Exiting", ros::this_node::getName().c_str());
        exit(1);
    }
    ros::Duration(0.3).sleep();
    if (!gripper_if->ackErr()){
        ROS_ERROR("[%s] Failed to send acknowledge to gripper", ros::this_node::getName().c_str());
    }
    ros::Duration(0.3).sleep();
    gripper_if->setDSA(params.use_dsa_ros);
    gripper_if->setDSASparsely(params.use_dsa_sparsely);

    // initialze ros interfaces as needed
    bool initialized{ros_if.init(gripper_if, nh)};
    if (params.use_all_wsg_services){
        full_srvs = std::make_shared<Wsg50RosSrvs>();
        initialized = initialized and full_srvs->init(gripper_if, nh);
    } else{ 
        base_srvs = std::make_shared<Wsg50RosBaseSrvs>();
        initialized = initialized and base_srvs->init(gripper_if, nh);        
    }
    if (!initialized){
        ROS_ERROR("[%s] Failed to set up ros interfaces. Exiting", ros::this_node::getName().c_str());
        exit(1);
    }

    // set up joint state publisher
    if (params.publish_joints){
        pub_joints = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::JointState>(params.joint_state_topic, 10));
        joint_states.header.frame_id = params.frame_id;
        assert(params.joints.size() > 1);
        joint_states.name.assign(params.joints.begin(), params.joints.begin() + 2);
        joint_states.position.resize(2);
        joint_states.velocity.resize(2);
        joint_states.effort.resize(2);
    }

    ROS_DEBUG("[WSG 50] Started gripper node: %s", params.info().c_str());
    auto prev = ros::Time::now();
    while(ros::ok()){
        auto now = ros::Time::now();
        auto dt = now - prev;
        ros_if.update(now, dt, params.gripper_name);
        if (params.publish_joints){
            joint_states.header.stamp = ros::Time::now();
            std::tie(joint_states.position[0], joint_states.position[1]) = gripper_if->getJointPosition();
            double s = (gripper_if->getCurrentSpeed());
            double f = (gripper_if->getMotorForce());
            joint_states.velocity[0] = s;
            joint_states.velocity[1] = s;
            joint_states.effort[0] = f;
            joint_states.effort[1] = f;
            pub_joints->publish(joint_states);
        }
        rate.sleep();
        prev = now;
    }
}
