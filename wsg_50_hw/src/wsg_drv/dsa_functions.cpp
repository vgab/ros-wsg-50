#include <ros/ros.h>
#include "wsg_50_hw/wsg_drv/dsa_functions.h"


float convert(const unsigned char *b) {
    float tmp;
    unsigned int src = 0;


    src = b[3] * 16777216 + b[2] * 65536 + b[1] * 256 + b[0];

    memcpy(&tmp, &src, sizeof tmp);

    return tmp;
}


int extractDSAmeasurement(auto *resp, auto off, auto dsa_finger0[DSA_ROWS][DSA_COLS],
                          auto dsa_finger1[DSA_ROWS][DSA_COLS]) {
    // extract WSG-DSA sensor array measurement
    // note: DSA matrix has 6 rows and 14 columns!
    // resp ... pointer to command response from WSG LUA script
    // off ... offset of resp to start of dsa matrix
    // dsa_finger ... matrices to extract to


    // this function extracts measurements incl. matrix transposition and flip up-down,
    // such that looking at the sensor results in [0][0] is the upper left corner, [13][5] is lower right corner
    for (int c = DSA_COLS - 1; c >= 0; --c) {
        for (int r = DSA_ROWS - 1; r >= 0; --r) {
            dsa_finger0[r][c] = convert(&resp[off]);
            dsa_finger1[r][c] = convert(&resp[off + (4 * DSA_ROWS * DSA_COLS)]);
            off += 4;
        }
    }
    return 0;
}


// for debugging only:
#include <iostream>
#include <iomanip>

template<typename T>
void print_matrix(T *array, int rows, int cols) {
    for (int r = 0; r < rows; ++r) {
        for (int c = 0; c < cols; ++c) {
            std::cout << std::setw(5) << array[r][c] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << '\n';
}


unsigned char sendCommand(unsigned char &cmd) {
    const unsigned char CMD_CUSTOM = 0xB0;
    return CMD_CUSTOM + cmd;
}

/**
 * Parse a ScriptCmd constant to the bit-command operator
 *  0xB0 -> read only
 *  0xB1 -> position control
 *  0xB2 -> speed control
 *  0xB3 -> dsa read & pos-control
 *  0xB4 -> dsa read & speed-control
 *  0xB5 -> dsa CoP read & pos-control
 *  0xB6 -> dsa CoP read & speed-control 
 * @param cmd
 * @return
 */
unsigned char sendCommand(const ScriptCmd &cmd) {
    const unsigned char CMD_CUSTOM = 0xB0;
    return CMD_CUSTOM + int(cmd);
}

/**
 *  Custom payload format:
 *  0:	Unused
 *  1:	float, target width, used for 0xB1 command
 *  5:	float, target speed, used for 0xB1 and 0xB2 command
 * @param cmd_width
 * @param cmd_speed
 * @param payload
 */
void prepare_payload(const float &cmd_width, const float &cmd_speed, unsigned char *payload) {
    payload[0] = 0x00;
    memcpy(&payload[1], &cmd_width, sizeof(float));
    memcpy(&payload[5], &cmd_speed, sizeof(float));
}

std::tuple<bool, std::string> check_status(unsigned char *resp) {
    auto status = cmd_get_response_status(resp);
    if (status == E_CMD_UNKNOWN)
        return std::make_tuple(false, "Command unknown - make sure script is running");
    if (status != E_SUCCESS)
        return std::make_tuple(false, "Command failed");
    return std::make_tuple(true, "");
}


std::tuple<std::string, bool, std::string> parse_status(const int &response_length,
                                                        unsigned char *resp,
                                                        const int &offset_status = 2) {

    try {
        if (response_length < offset_status)
            return std::make_tuple("", false, "Response too short to parse gripper status");
        unsigned char resp_state[6] = {0, 0, 0, 0, 0, 0};
        resp_state[2] = resp[offset_status];
        return std::make_tuple(std::string(getStateValues(resp_state)), true, "");
    } catch (...) {
        return std::make_tuple("", false, "unexpected error while parsing status");
    }
}


int parse_int8(unsigned char *resp, const int &idx) {
    uint8_t tmp{0};
    memcpy(&tmp, &resp[idx], sizeof(tmp));
    if (tmp > 0) {
        return (int) log2(tmp);
    }
    return 0;
}


int parse_int16(unsigned char *resp, const int &idx) {
    int out;
  	unsigned int src = 0;    
  	src = resp[idx+1] * 256 + resp[idx];
    memcpy(&out, &src, sizeof out);  	
	return out;
}


std::tuple<bool, std::string> parse_full_dsa(const int &response_length, unsigned char *resp, const int &offset_status,
                                             DsaData &dsa_data, bool use_int16=false) {
    int type_len{(use_int16) ? 2 : 4};    
    try {
        auto off{offset_status};
        if (response_length < offset_status + 2 * type_len * DSA_ROWS * DSA_COLS)
            return std::make_tuple(false, "Response too short to parse DSA data : " + std::to_string(response_length));
        for (auto &row: dsa_data.first) {
            for (auto &elem: row) {
                if (use_int16) {
                    elem = parse_int16(resp, off);
                } else {
                    elem = convert(&resp[off]);
                }
                off += type_len;
            }
        }
        for (auto &row: dsa_data.second) {
            for (auto &elem: row) {
                if (use_int16) {
                    elem = parse_int16(resp, off);
                } else {
                    elem = convert(&resp[off]);
                }
                off += type_len;
            }
        }
        return std::make_tuple(true, "");
    } catch (...){
        return std::make_tuple(false, "unexpected error while parsing DSA status");
    }
}



std::tuple<bool, std::string, std::pair<int, int>, std::pair<int, int>>
parse_dsa_cop_core(const int &response_length, unsigned char *resp, const int &offset) {
    std::pair<int, int> prsr{0, 0};
    std::pair<int, int> col_cnt{0, 0};

    try {
        if (response_length < offset + 10)
            return std::tuple(false, "insufficient data to parse DSA CoP data: " + std::to_string(response_length),
                              prsr, col_cnt);
        prsr.first = (int) convert(&resp[offset]);
        prsr.second = (int) convert(&resp[offset + 4]);
        col_cnt.first = parse_int8(resp, offset + 8);
        col_cnt.second = parse_int8(resp, offset + 9);
        return std::make_tuple(true, "", prsr, col_cnt);
    } catch (...) {
        return std::make_tuple(false, "unexpected error while parsing DSA status", prsr, col_cnt);
    }
}


bool parse_cop_data(const int &response_length, unsigned char *resp, const int &col_counter,
                    int &offset, std::pair<float, float> &cop_data, std::vector<std::array<int, 3>> &edges) {
    if (col_counter > 0) {
        int f_res_len{8 + col_counter * 3};
        if (offset + f_res_len > response_length) {
            ROS_ERROR("response mismatch for F0. Received %d, expected at least %d", response_length,
                      offset + f_res_len);
            return false;
        }
        cop_data.first = convert(&resp[offset]);
        cop_data.second = convert(&resp[offset + 4]);
        for (auto i=0; i < col_counter; i++){
            uint8_t y{0}, x_min{0}, x_max{0};
            memcpy(&y, &resp[offset + 8 + 3 * i], sizeof(y));
            memcpy(&x_min, &resp[offset +  9 + 3 * i], sizeof(x_min));
            memcpy(&x_max, &resp[offset + 10 + 3 * i], sizeof(x_max));
            edges.push_back(std::array<int,3>{(int) y, (int) x_min, (int) x_max});
        }
        offset += f_res_len;
    } else {
        cop_data.first = DSA_CENTER_X;
        cop_data.second = DSA_CENTER_Y;
    }
}


std::tuple<bool, std::string> parse_gripper_response(const int &response_length,
                                                     unsigned char *resp, const int &offset,
                                                     gripper_response &info) {
    auto[valid, err_msg] = check_status(resp);
    if (!valid || response_length < offset + 15) {
        return std::tuple(false, err_msg + "insufficient data to parse default gripper state: " +
                                 std::to_string(response_length));
    }
    auto offset_status{offset + 2};
    auto offset_pos{offset + 3};
    auto offset_speed{offset + 7};
    auto offset_force{offset + 11};
    std::tie(info.state_text, valid, err_msg) = parse_status(response_length, resp, offset_status);
    try {
        info.ismoving = (resp[offset_status] & 0x02) != 0;
        info.position = convert(&resp[offset_pos]);
        info.speed = convert(&resp[offset_speed]);
        info.f_motor = convert(&resp[offset_force]);
        return std::make_tuple(valid, err_msg);
    } catch (...) {
        return std::make_tuple(false, "unexpected error while parsing gripper info");
    }
}


int dsa_script_measure_move(const ScriptCmd &cmd_type, const float &cmd_width, const float cmd_speed,
                            gripper_response &info) {
    auto out_flag{CMD_ERROR};
    unsigned char payload[9];
    unsigned char *resp;
    unsigned int resp_len;
    prepare_payload(cmd_width, cmd_speed, payload);
    // Submit command and process result
    int res{0};
    try {
        res = cmd_submit(sendCommand(cmd_type), payload, 9, true, &resp, &resp_len);
        auto[valid, err_msg] = parse_gripper_response(res, resp, 0, info);
        if (!(valid)) {
            ROS_ERROR("[WSG 50 DRV] %s", err_msg.c_str());
        } else {
            out_flag = CMD_VALID;
        }
    } catch (...) {
        ROS_ERROR("unexpected error");
    }
    if (res > 0) free(resp);
    return out_flag;
}


int dsa_script_measure_move(const ScriptCmd &cmd_type, const float &cmd_width, const float cmd_speed,
                            gripper_response &info, DsaData &dsa_data) {
    if (int(cmd_type) <= int(ScriptCmd::SPEED_CTRL)) {
        return dsa_script_measure_move(cmd_type, cmd_width, cmd_speed, info);
    }
    auto out_flag = CMD_ERROR;
    unsigned char payload[9];
    unsigned char *resp;
    unsigned int resp_len;

    int res{0};
    try {
        prepare_payload(cmd_width, cmd_speed, payload);
        res = cmd_submit(sendCommand(cmd_type), payload, 9, true, &resp, &resp_len);
        if (res >= 15) {
            auto[valid_info, err_info] = parse_gripper_response(res, resp, 0, info);
            if (res == 23) {
                ROS_WARN ("Received response of length 23. Is FMF-finger activated? \n");
                out_flag = int(Finger::FMF);
            } else {
                if (res == 15 && valid_info) {
                    ROS_WARN("[WSG 50 DRV] no pressure data obtained");
                    out_flag = CMD_VALID;
                } else if (res == 570) {
                    ROS_WARN("[WSG 50 DRV] only received data of 570 bytes");
                } else {
                    auto[valid_dsa, err_dsa] = parse_full_dsa(res, resp, 15, dsa_data, true);
                    if (!(valid_info && valid_dsa)) {
                        ROS_ERROR("[WSG 50 DRV] %s", (err_info + err_dsa).c_str());
                    } else {
                        out_flag = CMD_VALID;
                    }
                }
            }
        }
        if (res > 0) free(resp);
    } catch (...){
        ROS_ERROR("unexpected error");
    } 
    return out_flag;
}


int dsa_script_measure_move(const ScriptCmd &cmd_type, const float &cmd_width, const float cmd_speed,
                            gripper_response &info,
                            std::pair<std::pair<float, float>, std::pair<float, float>> &dsa_cop,
                            std::pair<int, int> &dsa_prsr_sum,
                            std::pair<std::vector<std::array<int, 3>>, std::vector<std::array<int, 3>>> &edges) {
    if (int(cmd_type) <= int(ScriptCmd::SPEED_CTRL)) {
        return dsa_script_measure_move(cmd_type, cmd_width, cmd_speed, info);
    }
    auto out_flag = CMD_ERROR;
    unsigned char payload[9];
    unsigned char *resp;
    unsigned int resp_len;

    int res{0};
    try {
        prepare_payload(cmd_width, cmd_speed, payload);
        res = cmd_submit(sendCommand(cmd_type), payload, 9, true, &resp, &resp_len);
        if (res >= 15) {
            auto[valid_info, err_info] = parse_gripper_response(res, resp, 0, info);
            if (!valid_info) {
                ROS_ERROR("[WSG 50 DRV] corrupt data: %s", err_info.c_str());
            } else if (res == 15) {
                ROS_WARN("[WSG 50 DRV] no pressure data obtained");
                out_flag = CMD_VALID;
            }
            std::pair<int, int> f_has_data{};
            std::tie(valid_info, err_info, dsa_prsr_sum, f_has_data) = parse_dsa_cop_core(res, resp, 15);
            int cur_offset{25};
            if (!valid_info) {
                ROS_ERROR("[WSG 50 DRV] corrupt DSA data: %s", err_info.c_str());
            }
            if (parse_cop_data(res, resp, f_has_data.first,  cur_offset, dsa_cop.first,  edges.first) &&
                parse_cop_data(res, resp, f_has_data.second, cur_offset, dsa_cop.second, edges.second)){
            }
            if (cur_offset == res) {
                out_flag = CMD_VALID;
            } else{
                ROS_ERROR("Expected a response of length %d, received %d instead", cur_offset, res);
            }
            ROS_DEBUG_STREAM(
                    "obtained sparse DSA-data (#" << res << "):"
                                       << "\n\tsum:   \t  " << dsa_prsr_sum.first << ", " << dsa_prsr_sum.second
                                       << "\n\t#cols: \t" << f_has_data.first << ", " << f_has_data.second
                                       << "\n\tCoP F0:\t" << dsa_cop.first.first  << ", " << dsa_cop.first.second
                                       << "\n\tCoP F1:\t" << dsa_cop.second.first << ", " << dsa_cop.second.second
                                       );
        }
        if (res > 0) free(resp);
    } catch (...) {
        ROS_ERROR("unexpected error");
    }
    return out_flag;
}

