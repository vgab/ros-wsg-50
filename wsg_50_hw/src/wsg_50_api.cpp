#include <wsg_50_hw/wsg_50_api.h>


namespace wsg_50 {

    const std::string write_log_name{"write"};
    const std::string dsa_log_name{"dsa"};

    // helper functions / structs
    struct WsgConnectParams {
        std::string ip{"192.168.1.20"};
        std::string protocol{"tcp"};
        std::string com_mode{"script"};
        int auto_update_ms{0};
        int port{1000};
        int local_port{1501};

        explicit WsgConnectParams(const std::string &p_n = "") {
            ros::NodeHandle nh{"~"};
            const std::string &base_param{p_n};
            nh.param<std::string>(base_param + "ip", ip, ip);
            nh.param<std::string>(base_param + "protocol", protocol, protocol);
            nh.param<std::string>(base_param + "com_mode", com_mode, com_mode);
            nh.param<int>(base_param + "auto_update_ms", auto_update_ms, auto_update_ms);
            nh.param<int>(base_param + "port", port, port);
            nh.param<int>(base_param + "local_port", local_port, local_port);
        }

        [[nodiscard]] inline std::string tcp_info() const {
            std::stringstream tmp;
            tmp << "\n\tip:            \t" << ip
                << "\n\tport:          \t" << port
                << "\n\tprotocol:      \t" << protocol
                << "\n\tcom_mode:      \t" << com_mode;
            return tmp.str();
        }

        [[nodiscard]] inline std::string udp_info() const {
            std::stringstream tmp;
            tmp << "\n\tlocal port:     \t" << port;
            return tmp.str() + tcp_info();
        }

    };

    ComMode parseComStr(const std::string &com_mode) {
        if (com_mode == "script") {
            return ComMode::SCRIPT;
        } else if (com_mode == "auto_update") {
            return ComMode::AUTO;
        } else if (com_mode == "polling") {
            return ComMode::POLL;
        } else {
            return ComMode::UNKNOWN;
        }
    }

    Protocol parseProtStr(const std::string &protocol) {
        if (protocol == "udp")
            return Protocol::UDP;
        else
            return Protocol::TCP;
    }

    // class functions
    Wsg50API::~Wsg50API() {
        if (m_com_mode == ComMode::AUTO) {
            getOpening(0);
            getSpeed(0);
            getForce(0);
        }
        cmd_disconnect();
    }

    bool Wsg50API::connect(const ros::NodeHandle &nh, const std::string &param_ns,
                           const InterfaceMode &if_mode) {
        // Connect to device using TCP/USP
        if (m_initialized) {
            return true;
        }
        m_if_mode = if_mode;
        m_cmd_blocked = false;
        assert (m_if_mode == InterfaceMode::ETH);
        WsgConnectParams params{param_ns};
        m_com_mode = parseComStr(params.com_mode);
        auto info = params.tcp_info();
        if (parseProtStr(params.protocol) == Protocol::TCP) {
            cmd_connect_tcp(params.ip.c_str(), params.port);
        } else {
            cmd_connect_udp(params.local_port, params.ip.c_str(), params.port);
            info = params.udp_info();
        }
        m_initialized = cmd_is_connected();
        if (m_initialized) {
            ROS_INFO("[%s] connected to WSG gripper:%s", m_log_name.c_str(), info.c_str());
        } else {
            ROS_ERROR("[%s] failed to connect to WSG gripper:%s", m_log_name.c_str(), info.c_str());
            return false;
        }
        if (m_com_mode == ComMode::SCRIPT) {
            assert (m_if_mode == InterfaceMode::ETH);
        } else if (m_com_mode == ComMode::AUTO) {
            m_auto_update_ms = params.auto_update_ms;
            assert(m_auto_update_ms > 0.0);
            assert(m_if_mode == InterfaceMode::ETH);
            getOpening(m_auto_update_ms);
            getSpeed(m_auto_update_ms);
            getForce(m_auto_update_ms);
            m_th = std::thread(&Wsg50API::periodicRead, this);
        }
        read();
        m_w_des = m_cur_width;
        m_s_des = m_cur_speed;
        m_speed_des = m_cur_speed;
        return m_initialized;
    }

    void Wsg50API::set_gripper_data(const GripperResponse &gripper_data) {
        m_cur_width = gripper_data.position;
        m_cur_speed = gripper_data.speed;
        m_f_motor = gripper_data.f_motor;
        m_f_finger.first = gripper_data.f_finger0;
        m_f_finger.second = gripper_data.f_finger1;
        m_state_text = gripper_data.state_text;
        m_is_moving = gripper_data.ismoving;
    }

    void Wsg50API::read() {
        assert(m_initialized);
        if (checkAccess()) {
            if (m_com_mode == ComMode::SCRIPT) {
                script_msr_wrapper(ScriptCmd::READ_ONLY);
            } else if (m_com_mode == ComMode::POLL) {
                m_cmd_blocked = true;
                m_cur_width = getOpening();
                m_cur_speed = getSpeed();
                m_f_motor = getForce();
                m_cur_acc = getAcceleration();
                const char *state = systemState();
                if (!state)
                    return;
                m_state_text = std::string(state);
                m_cmd_blocked = false;
            } else if (m_com_mode == ComMode::AUTO) {
                ROS_ERROR("Gripper in auto mode, explicit update calls are not expected");
            }
        }
    }

    bool Wsg50API::readOnly() const {
        if (m_reset_cmds) {
            return true;
        }
        if (m_com_mode == ComMode::SCRIPT) {
            if (m_speed_ctrl) {
                return std::abs(m_speed_des - m_cur_speed) < m_speed_tolerance;
            } else {
                return std::abs(m_w_des - m_cur_width) < m_pos_tolerance;
            }
        } else {
            return true;
        }
    }

    void Wsg50API::script_msr_wrapper(const ScriptCmd &cmd_type) {
        GripperResponse gripper_data{};
        auto width_des{m_w_des};
        auto speed_des{m_s_des};
        if (m_speed_ctrl) {
            width_des = 0.;
            speed_des = m_speed_des;
            ROS_DEBUG_THROTTLE_NAMED(1.0, write_log_name, "[%s] speed control to %.3f [mm/s] ", m_log_name.c_str(), speed_des);
        } else {
            ROS_DEBUG_THROTTLE_NAMED(1.0, write_log_name, "[%s] position control to %.3f [mm] with speed %.3f [mm/s]", m_log_name.c_str(), m_w_des,
                      m_s_des);
        }        
        int ret_val{0};
        m_cmd_blocked = true;
        if (m_dsa_request || m_dsa_enabled){
            try{
                if (m_dsa_sparse_request){
			m_dsa_surface_edge.first.clear();
	                m_dsa_surface_edge.second.clear();
                    if (m_speed_ctrl){
        	            ret_val = dsa_script_measure_move(ScriptCmd::READ_DSA_COP_SPD_CTRL, width_des, speed_des, gripper_data, m_dsa_cop, m_dsa_sum, m_dsa_surface_edge);        
                    } else{
                        ret_val = dsa_script_measure_move(ScriptCmd::READ_DSA_COP, width_des, speed_des, gripper_data, m_dsa_cop, m_dsa_sum, m_dsa_surface_edge);        
                    }
                } else {
                    if (m_speed_ctrl){
                        ret_val = dsa_script_measure_move(ScriptCmd::READ_DSA_SPD_CTRL, width_des, speed_des, gripper_data, m_dsa_finger);
                    } else{
                        ret_val = dsa_script_measure_move(ScriptCmd::READ_DSA, width_des, speed_des, gripper_data, m_dsa_finger);
                    }
                }
                if (ret_val == CMD_VALID) {
                    ROS_DEBUG_THROTTLE_NAMED(1.0, dsa_log_name, "[%s] Processed DSA request", m_log_name.c_str());
                    m_dsa_request = false;
                    if (!m_dsa_enabled)
                        m_dsa_sparse_request = false;
                } else{
                    ROS_DEBUG_THROTTLE_NAMED(1.0, dsa_log_name, "[%s] Failed to process DSA request", m_log_name.c_str());
                }
            }catch (...){
                ROS_DEBUG_THROTTLE_NAMED(1.0, dsa_log_name, "[%s] failed reading data ", m_log_name.c_str());
            }
        } else{
            ret_val = dsa_script_measure_move(cmd_type, width_des, speed_des, gripper_data);
        }
        m_cmd_blocked = false;
        if (ret_val == CMD_VALID) {
            set_gripper_data(gripper_data);
        } else if (ret_val == int(Finger::FMF) && m_gripper_finger != Finger::FMF) {
            m_gripper_finger = Finger::FMF;
        } else if (ret_val == int(Finger::DSA) && m_gripper_finger != Finger::DSA) {
            m_gripper_finger = Finger::DSA;
        }
    }

    void Wsg50API::update() {
        if (readOnly()) {
            read();
            if (m_reset_cmds) {
                m_w_des = m_cur_width;
                m_s_des = m_cur_speed;
                m_speed_des = m_cur_speed;
                m_reset_cmds = false;
            }
        }
        if (checkAccess()) {
            if (m_com_mode == ComMode::SCRIPT) {
                if (m_speed_ctrl) {
                    script_msr_wrapper(ScriptCmd::SPEED_CTRL);
                } else {
                    script_msr_wrapper(ScriptCmd::POS_CTRL);
                }
            } else if (m_com_mode == ComMode::POLL) {
                read();
                ROS_WARN("[%s] Writing commands to gripper is not implemented in polling mode", m_log_name.c_str());
            } else if (m_com_mode == ComMode::AUTO) {
                ROS_ERROR("[%s] Gripper in auto mode, explicit update calls are not expected", m_log_name.c_str());
            }
        } else {
            ROS_DEBUG("[%s] Access to gripper blocked by parallel command", m_log_name.c_str());
        }
    }

    bool Wsg50API::checkAccess() const {
        if (!m_initialized) {
            ROS_ERROR("[%s] connection not established", m_log_name.c_str());
            return false;
        }
        return !m_cmd_blocked;
    }

    bool Wsg50API::waitForAccess() {
         if (!m_initialized) {
            ROS_ERROR("[%s] connection not established", m_log_name.c_str());
            return false;
        }
        auto t0 = ros::Time::now();
        while ((ros::Time::now() - t0).toSec() < 1.0){
            if (!m_cmd_blocked){
                m_cmd_blocked = true;
                return true;
            }
            m_cmd_retry_rate.sleep();
        }
        ROS_ERROR("[%s] access request timeout", m_log_name.c_str());
        return false;
    }

    bool Wsg50API::setForceLimit(const float &force) {
        if (force == m_f_max)
            return true;
        if (force < 0.0 || 80.0 < force) {
            ROS_ERROR("[%s] force limit is [0, 80.0]. Received:%f", m_log_name.c_str(), force);
            return false;
        }
        if (waitForAccess()) {
            bool out{setGraspingForceLimit(force) >= 0};
            m_cmd_blocked = false;
            if (out)
                m_f_max = force;
            return out;
        }
        return false;
    }

    bool Wsg50API::setAcc(const float &acc) {
        if (acc == m_acc_max)
            return true;
        if (acc < 0.0) {
            ROS_ERROR("[%s] negative acceleration is not supported", m_log_name.c_str());
            return false;
        }
        if (waitForAccess()) {
	    bool out{false};
            try {
                out = setAcceleration(acc) >= 0;
            } catch (...) {m_cmd_blocked = false;}
            m_cmd_blocked = false;
            if (out)
                m_acc_max = acc;
            return out;
        }
        return false;
    }

    bool Wsg50API::ackErr() {
        if (waitForAccess()) {
            try {
                bool out{ack_fault() >= 0};
                m_cmd_blocked = false;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    bool Wsg50API::stopCmd(const bool &ignore_response) {
        if (waitForAccess()) {
            try {
                bool out{stop(ignore_response) >= 0};
                m_cmd_blocked = false;
                m_reset_cmds = true;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    bool Wsg50API::homingCmd() {
        if (waitForAccess()) {
            try {
                bool out{homing() >= 0};
                m_cmd_blocked = false;
                m_reset_cmds = true;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    bool Wsg50API::releaseCmd(const float &width, const float &speed) {
        if (waitForAccess()) {
            try {
                bool out{release(width, speed) >= 0};
                m_cmd_blocked = false;
                m_reset_cmds = true;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    bool Wsg50API::graspCmd(const float &width, const float &speed) {
        if (waitForAccess()) {
            try {
                bool out{grasp(width, speed) >= 0};
                m_cmd_blocked = false;
                m_reset_cmds = true;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    bool Wsg50API::moveCmd(const float &width, const float &speed,
                           const bool &stop_on_block, const bool &ignore_response) {
        if (waitForAccess()) {
            try{
                bool out{move(width, speed, stop_on_block, ignore_response) >= 0};
                m_cmd_blocked = false;
                m_reset_cmds = false;
                return out;
            } catch (...) {m_cmd_blocked = false;}
        }
        return false;
    }

    void Wsg50API::periodicRead() {
        assert(m_com_mode == ComMode::POLL);
        throw std::runtime_error("this function is deprecated and needs reimplementation");
        /* code below kept as a reference
        status_t status;
        int res;
        bool pub_state = false;

        double rate_exp = 1000.0 / (double) m_auto_update_ms;
        std::string names[3] = {"opening", "speed", "force"};

        // Prepare messages
        wsg_50_msgs::Status status_msg;
        status_msg.status = "UNKNOWN";

        sensor_msgs::JointState joint_states;
        joint_states.header.frame_id = "wsg_50_gripper_base_link";
        joint_states.name.push_back("wsg_50_gripper_base_joint_gripper_left");
        joint_states.name.push_back("wsg_50_gripper_base_joint_gripper_right");
        joint_states.position.resize(2);
        joint_states.velocity.resize(2);
        joint_states.effort.resize(2);

        // Request automatic updates (error checking is done below)
        getOpening(interval_ms);
        getSpeed(interval_ms);
        getForce(interval_ms);


        msg_t msg;
        msg.id = 0;
        msg.data = 0;
        msg.len = 0;
        int cnt[3] = {0, 0, 0};
        auto time_start = std::chrono::system_clock::now();


            // Receive gripper response
            msg_free(&msg);
            res = msg_receive(&msg);
            if (res < 0 || msg.len < 2) {
                ROS_ERROR("Gripper response failure: too short");
                continue;
            }

            float val = 0.0;
            status = cmd_get_response_status(msg.data);

            // Decode float for opening/speed/force
            if (msg.id >= 0x43 && msg.id <= 0x45 && msg.len == 6) {
                if (status != E_SUCCESS) {
                    ROS_ERROR("Gripper response failure for opening/speed/force\n");
                    continue;
                }
                val = convert(&msg.data[2]);
            }

            // Handle response types
            int motion = -1;
            switch (msg.id) {
                // Opening
                case 0x43:
                    status_msg.width = val;
                    pub_state = true;
                    cnt[0]++;
                    break;
                // Speed
                case 0x44:
                    status_msg.speed = val;
                    cnt[1]++;
                    break;

                // Force
                case 0x45:
                    status_msg.force = val;
                    cnt[2]++;
                    break;

                // Move
                // Move commands are sent from outside this thread
                case 0x21:
                    if (status == E_SUCCESS) {
                        float diff;
                        diff = status_msg.width - g_goal_position;
                        if (diff > 0.6 || diff < -0.6) {
                            g_goal_position++; // to invoke new move command from callback
                            ROS_INFO("Position reached: error=%5.4fmm, will try again...", diff);
                        } else {
                            ROS_INFO("Position reached: pos=%5.1f\n", g_goal_position);
                        }
                        motion = 0;
                    } else if (status == E_AXIS_BLOCKED) {
                        ROS_INFO("Axis blocked");
                        motion = 0;
                    } else if (status == E_CMD_PENDING) {
                        ROS_INFO("Movement started");
                        motion = 1;
                    } else if (status == E_ALREADY_RUNNING) {
                        ROS_INFO("Movement error: already running");
                    } else if (status == E_CMD_ABORTED) {
                        ROS_INFO("Movement aborted");
                        motion = 0;
                    } else {
                        ROS_INFO("Movement error, will try again...");
                        g_goal_position++; // to invoke new move command from callback
                        motion = 0;
                    }
                    break;

                // Stop
                // Stop commands are sent from outside this thread
                case 0x22:
                    // Stop command; nothing to do
                    break;
                default:
                    ROS_INFO("Received unknown respone 0x%02x (%2dB)\n", msg.id, msg.len);
            }

            // PUBLISH motion message
            if (motion == 0 || motion == 1) {
                std_msgs::Bool moving_msg;
                moving_msg.data = motion;
                g_pub_moving.publish(moving_msg);
                g_ismoving = motion;
            }

            // ***** PUBLISH state message & joint message
            if (pub_state) {
                pub_state = false;
                g_pub_state.publish(status_msg);

                joint_states.header.stamp = ros::Time::now();;
                joint_states.position[0] = -status_msg.width / 2000.0;
                joint_states.position[1] = status_msg.width / 2000.0;
                joint_states.velocity[0] = status_msg.speed / 1000.0;
                joint_states.velocity[1] = status_msg.speed / 1000.0;
                joint_states.effort[0] = status_msg.force;
                joint_states.effort[1] = status_msg.force;
                g_pub_joint.publish(joint_states);
            }

            // Check # of received messages regularly
            std::chrono::duration<float> t = std::chrono::system_clock::now() - time_start;
            double t_ = t.count();
            if (t_ > 5.0) {
                time_start = std::chrono::system_clock::now();
                //printf("Infos for %5.1fHz, %5.1fHz, %5.1fHz\n", (double)cnt[0]/t_, (double)cnt[1]/t_, (double)cnt[2]/t_);

                std::string info = "Rates for ";
                for (int i = 0; i < 3; i++) {
                    double rate_is = (double) cnt[i] / t_;
                    info += names[i] + ": " + std::to_string((int) rate_is) + "Hz, ";
                    if (rate_is == 0.0)
                        ROS_ERROR("Did not receive data for %s", names[i].c_str());
                }
                ROS_DEBUG_STREAM((info + " expected: " + std::to_string((int) rate_exp) + "Hz").c_str());
                cnt[0] = 0;
                cnt[1] = 0;
                cnt[2] = 0;
            }
        }
        */
    }

}

