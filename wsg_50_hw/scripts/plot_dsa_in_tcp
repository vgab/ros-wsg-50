#!/usr/bin/env python
import click
import numpy as np
import matplotlib.pylab as plt
from matplotlib.animation import FuncAnimation

import rospy

from wsg_50_hw import DsaGripperCommandHandle
from wsg_50_hw.plotting import init_figure, print_error, animate_dsa_tcp

# init node
rospy.init_node("wsg_50_dsa_plotter")

# get params
gripper_ns = rospy.get_param('gripper_ns', '/wsg_50_hw')
loop_hz = rospy.get_param('update_hz', 25.0)
cop_data_only = rospy.get_param('cop_data_only', False)
plot_err = rospy.get_param('plot_error', True)


# start loop
@click.command(context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--gripper_node", default=gripper_ns, help="WSG 50 ROS node name. Defaults to {}".format(gripper_ns))
@click.option("--cop-only/--no-cop-only", default=cop_data_only, help=". Defaults to {}".format(cop_data_only))
@click.option("--plot-error/--no-plot-error", default=plot_err,
              help="Print error . Defaults to {}".format(plot_err))
def run_dsa_plotter(gripper_node, cop_only, plot_error):
    gripper = DsaGripperCommandHandle(gripper_node)
    gripper.enable_dsa(cop_only)

    base_title = 'alignment error estimation visualization in TCP frame'

    fig, axs = init_figure()
    txt_handle = print_error(fig, np.zeros(6)) if plot_error else None
    anim = FuncAnimation(fig, animate_dsa_tcp, fargs=(fig, axs, gripper, txt_handle, base_title), interval=200)
    plt.show()

    rospy.spin()


if __name__ == "__main__":
    run_dsa_plotter()
