//
// Created by gabler on 27.01.21.
//

#ifndef WSG_50_HW_CONSTANTS_H
#define WSG_50_HW_CONSTANTS_H

#define GRIPPER_MAX_OPEN (float) 110.0
#define GRIPPER_MIN_OPEN (float) 0.0
#define GRIPPER_MAX_SPEED (float) 420.0
#define GRIPPER_MIN_SPEED (float) 0.1
#define DSA_ROWS 14
#define DSA_COLS 6
#define CMD_ERROR 0
#define CMD_VALID 1

#define DEBUG false

enum class InterfaceMode{
    ETH,
    CAN
};

enum class ComMode{
    POLL,
    SCRIPT,
    AUTO,
    UNKNOWN
};

enum class Protocol{
    TCP,
    UDP
};

enum class Finger{
    NONE=1,
    FMF=2,
    DSA=3
};


enum class ScriptCmd{
    READ_ONLY = 0,
    POS_CTRL = 1,
    SPEED_CTRL = 2,
    READ_DSA = 3,        
    READ_DSA_SPD_CTRL = 4,
    READ_DSA_COP = 5,
    READ_DSA_COP_SPD_CTRL = 6
};





#endif //WSG_50_HW_CONSTANTS_H
