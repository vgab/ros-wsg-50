#ifndef WSG_50_HW_WSG_HW_IF_H
#define WSG_50_HW_WSG_HW_IF_H

#include <thread>
#include <ros/ros.h>
#include "wsg_50_hw/wsg_drv/constants.h"
#include "wsg_50_hw/wsg_drv/common.h"
#include "wsg_50_hw/wsg_drv/cmd.h"
#include "wsg_50_hw/wsg_drv/msg.h"
#include "wsg_50_hw/wsg_drv/dsa_functions.h"
#include "wsg_50_hw/wsg_drv/functions.h"

namespace wsg_50 {

    typedef gripper_response GripperResponse;
    typedef std::pair<DsaMat, DsaMat> DsaData;

    class Wsg50API {

    public:
        Wsg50API() = default;

        ~Wsg50API();

        bool connect(const ros::NodeHandle &nh, const std::string& param_ns="",
                     const InterfaceMode &if_mode=InterfaceMode::ETH);

        void read();

        void update();

        //getter
        [[nodiscard]] bool processCmd() const { return m_cmd_blocked; };

        [[nodiscard]] bool dsaRequestIsProcessed() const { return !m_dsa_request; };

        [[nodiscard]] std::string getStatus() const { return m_state_text; };

        [[nodiscard]] std::pair<float, float> getJointPosition() const {
            return std::make_pair(-m_cur_width / 2000.0, m_cur_width / 2000.0);
        };

        [[nodiscard]] std::pair<float, float> getDSACop(const int &f_num) const {
            if (f_num == 1)
                return m_dsa_cop.second;
           return m_dsa_cop.first;
        }

        [[nodiscard]]  std::vector<std::array<int, 3>> getDSAEdges (const int &f_num) const {
            if (f_num == 1)
                return m_dsa_surface_edge.second;
            return m_dsa_surface_edge.first;
        }

        [[nodiscard]]  int getDSAPressureSum (const int &f_num) const {
            if (f_num == 1)
                return m_dsa_sum.second;
            return m_dsa_sum.first;
        }

        [[nodiscard]] float getOpenWidth() const { return m_cur_width / (float(1000.0)); };

        [[nodiscard]] float getCurrentSpeed() const{ return m_cur_speed / (float(1000.0)); };

        [[nodiscard]] float getAcceleration() const { return m_cur_acc; };

        [[nodiscard]] float getMotorForce() const { return m_f_motor; };

        [[nodiscard]] bool isMoving() const { return m_is_moving; };

        [[nodiscard]] std::pair<float, float> getFingerForce() { return m_f_finger; };

        [[nodiscard]] DsaData getDsaDataPtr() { return m_dsa_finger; };

        // setter API
        void requestDSA()  { m_dsa_request = true; };

        void requestSparseDSA()  { m_dsa_request = true; m_dsa_sparse_request = true;};

        bool setAcc(const float &acc);

        bool setForceLimit(const float &force);

        inline bool setWidthCmd(const float &w, const float &s) {
            m_w_des = w;
            m_s_des = s;
            m_speed_ctrl = false;
            return true;
        };

        inline bool setSpeedCmd(const float &s) {
            m_speed_des = s;
            m_speed_ctrl = true;
            return true;
        };

        inline void setDSA(const bool &flag){m_dsa_enabled = flag;};

        inline void setDSASparsely(const bool &flag){m_dsa_sparse_request = flag;};
        // command API
        [[nodiscard]] bool homingCmd();

        [[nodiscard]] bool stopCmd(const bool &ignore_response = false);

        [[nodiscard]] bool ackErr();

        [[nodiscard]] bool releaseCmd(const float &width, const float &speed);

        [[nodiscard]] bool graspCmd(const float &width, const float &speed);

        [[nodiscard]]  bool moveCmd(const float &width, const float &speed,
                                    const bool &stop_on_block, const bool &ignore_response = false);

        [[nodiscard]] bool isConnected() const { return m_initialized; };

    private:
        const std::string m_log_name{"WSG_50_api"};
        InterfaceMode m_if_mode{InterfaceMode::ETH};
        ComMode m_com_mode{ComMode::SCRIPT};
        Finger m_gripper_finger{Finger::NONE};
        ros::Rate m_cmd_retry_rate{1e4};
        std::string m_state_text;
        std::pair<float, float> m_f_finger{0., 0.};
        std::pair<int, int> m_dsa_sum{};
        std::pair<std::pair<float, float>, std::pair<float, float>> m_dsa_cop{};
        std::pair<std::vector<std::array<int, 3>>, std::vector<std::array<int, 3>>> m_dsa_surface_edge{};
        std::thread m_th{};
        DsaData m_dsa_finger{};
        const float m_speed_tolerance{1e-1};
        const float m_pos_tolerance{1e-1};
        float m_w_des{0.0};
        float m_s_des{0.0};
        float m_speed_des{0.0};
        float m_f_motor{0.};
        float m_cur_width{0.0};
        float m_cur_speed{0.0};
        float m_cur_acc{0.0};
        float m_acc_max{0.0};
        float m_f_max{0.0};
        int m_auto_update_ms{0};
        bool m_initialized{false};
        bool m_is_moving{false};
        bool m_cmd_blocked{false};
        bool m_dsa_request{false};
        bool m_dsa_enabled{false};
        bool m_dsa_sparse_request{false};
        bool m_reset_cmds{false};
        bool m_speed_ctrl{false};

        void periodicRead();

        void set_gripper_data(const GripperResponse &gripper_data);

        void script_msr_wrapper(const ScriptCmd& cmd_type);

        [[nodiscard]] bool checkAccess() const;

        [[nodiscard]] bool waitForAccess();

        [[nodiscard]] bool readOnly() const;

    };
}
#endif //WSG_50_HW_WSG_HW_IF_H
