#ifndef WSG_50_HW_WSG_50_ROS_H
#define WSG_50_HW_WSG_50_ROS_H

#include <thread>

#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"

#include <realtime_tools/realtime_publisher.h>

#include <wsg_50_hw/wsg_50_api.h>
#include "wsg_50_msgs/Status.h"
#include "wsg_50_msgs/StatusDSA.h"
#include "wsg_50_msgs/StatusFull.h"
#include "wsg_50_msgs/DsaData.h"
#include "wsg_50_msgs/DsaCop.h"
#include "wsg_50_msgs/GetDsa.h"
#include "wsg_50_msgs/GetDsaCop.h"
#include "wsg_50_msgs/Move.h"
#include "wsg_50_msgs/Conf.h"
#include "wsg_50_msgs/Incr.h"
#include "wsg_50_msgs/Cmd.h"
#include "wsg_50_msgs/SetDsa.h"


namespace wsg_50{

    /**
     * @brief convert a Dsa Matrix message to a ros-message
     *
     * @param dsa_data current dsa matrix reading
     * @return ros-message for dedicated matrix
     */
    wsg_50_msgs::DsaData asMsg(const DsaMat &dsa_data);

    /**
     * @brief Get the Status Msg object
     *
     * @param hw_api gripper interface handle
     * @return wsg_50_msgs::StatusFull status including dsa data
     */
    wsg_50_msgs::StatusFull getStatusFullMsg(const std::shared_ptr<wsg_50::Wsg50API> &hw_api);

    /**
     * @brief Get the Status Msg object
     *
     * @param hw_api gripper interface handle
     * @return wsg_50_msgs::Status  status message
     */
    wsg_50_msgs::Status getStatusMsg(const std::shared_ptr<wsg_50::Wsg50API> &hw_api);


    /**
     * @brief A simple class that allows to wrap base services (acknowledge, homing, dyn limits for reset)
     * services available in the web-interface to the ROS-world
     *
     */
    class Wsg50RosBaseSrvs{

    public:
        Wsg50RosBaseSrvs() = default;

        virtual bool init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh);

    protected:
        std::shared_ptr<wsg_50::Wsg50API> m_hw_api{};

    private:
        const std::string m_log_name{"WSG_50_ros_srvs"};
        ros::ServiceServer m_homing_srv{};
        ros::ServiceServer m_ack_srv{};
        ros::ServiceServer m_set_acc_srv{};
        ros::ServiceServer m_set_force_lim_srv{};

        /**
         * @brief ROS-release service for internal WSG gripper homing service with predefined home width
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool homingSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

        /**
        * @brief ROS-release service to set desired acceleration value for motions
        * @param req service request with desired acceleration value
        * @param res service response error code if encountered
        * @return true if successful
        */
        bool setAccSrv(wsg_50_msgs::Conf::Request &req, wsg_50_msgs::Conf::Response &res);

        /**
        * @brief ROS-release service to set desired force limit during grasping motions
        * @param req service request with desired force limit
        * @param res service response error code if encountered
       * @return true if successful
        */
        bool setForceSrv(wsg_50_msgs::Conf::Request &req, wsg_50_msgs::Conf::Response &res);

        /**
         * @brief send acknowledge command to gripper
         * @param req empty
         * @param res empty
         * @return true if successful
         */
        bool ackSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

    };


    /**
     * @brief A simple class that allows to wrap all default
     * services available in the web-interface to the ROS-world
     *
     */
    class Wsg50RosSrvs:public Wsg50RosBaseSrvs{

    public:
        bool init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh) override;

    private:
        const std::string m_log_name{"WSG_50_ros_srvs"};
        std::shared_ptr<wsg_50::Wsg50API> m_hw_api{};

        bool m_object_grasped{false};

        ros::ServiceServer m_move_incr_srv{};
        ros::ServiceServer m_move_srv{};
        ros::ServiceServer m_grasp_srv{};
        ros::ServiceServer m_release_srv{};        
        ros::ServiceServer m_stop_srv{};                

        std::tuple<bool, uint> moveWidthSpeed(float &width, float &speed);

        /**
         * @brief Ros- service for WSG grasping service
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool graspSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res);

        /**
         * @brief Ros- service for incremental open / close WSG service
         *
         *
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool incrementSrv(wsg_50_msgs::Incr::Request &req, wsg_50_msgs::Incr::Response &res);

        /**
         * @brief ROS-release service for internal WSG gripper release service
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool releaseSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res);

        /**
         * @brief ROS-release service for internal WSG gripper move service with set point and velocity
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool moveSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res);

        /**
         * @brief ROS-release service for internal WSG gripper stop service bringing gripper to halt
         * @param req service request
         * @param res service response
         * @return true if successful
         */
        bool stopSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

    };

    class Wsg50Ros{

    public:
        Wsg50Ros() = default;

        /**
         * @brief initialize main Ros node
         * 
         * @param hw shared WSG driver pointer
         * @param nh ros node handle
         * @return true if init is successful
         * @return false if connection fails
         */
        bool init(std::shared_ptr<wsg_50::Wsg50API> hw, ros::NodeHandle &nh);

        /**
         * @brief update gripper in a loopy manner
         *
         * @param time  current time
         * @param period time delta
         * @param gripper_name base name for frame-id in DSA status
         * @return true
         */
        bool update(const ros::Time &time, const ros::Duration &period, const std::string &gripper_name="wsg_50");

        [[nodiscard]] wsg_50_msgs::Status getStatusMsg ();


    private:
        const std::string m_log_name{"WSG_50_ros"};
        std::shared_ptr<wsg_50::Wsg50API> m_hw_api{};

        //services
        ros::ServiceServer m_get_dsa_data_srv{};
        ros::ServiceServer m_get_dsa_cop_srv{};

        //subscribers
        std::vector<ros::Subscriber> m_subs;

        // status
        std::shared_ptr<realtime_tools::RealtimePublisher<wsg_50_msgs::StatusFull>> m_pub_status{};

        float m_dsa_hz{2.}, m_loop_hz{50.};
        bool m_asynchronous{false};
        bool m_read_dsa{false};
        bool m_read_dsa_sparsely{false};

        /**
         * @brief set position and velocity command to current ros-instance
         *
         * @param msg command message with position and speed command
         */
        void position_cb(const wsg_50_msgs::Cmd::ConstPtr &msg);

        /**
         * @brief set movement speed for current robot
         *
         * @param msg command message with position and speed command
         */
        void speed_cb(const std_msgs::Float32::ConstPtr &msg);

        /**
         * @brief enable dsa data reading
         *
         * @param msg new DSA read flag as a message
         */
        void setDSAcb(const wsg_50_msgs::SetDsa::ConstPtr &msg);

        /**
         * @brief get DSA data reading as a service
         *
         * @param req empty request
         * @param res contains flattened data arrays for both fingers
         * @return true if successful
         */
        bool getDsaSrv(wsg_50_msgs::GetDsa::Request &req, wsg_50_msgs::GetDsa::Response &res);

        /**
         * @brief get center of pressure for current DSA sensor fingers
         *
         * @param req empty request
         * @param res contains center of pressure in x,y area and
         * @return true if successful
         */
        bool getDsaCopSrv(wsg_50_msgs::GetDsaCop::Request &req, wsg_50_msgs::GetDsaCop::Response &res);
    };
} // namespace
#endif //WSG_50_HW_WSG_50_ROS_H
